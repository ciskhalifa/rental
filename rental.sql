/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - rental
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rental` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rental`;

/*Table structure for table `detail_pinjam` */

DROP TABLE IF EXISTS `detail_pinjam`;

CREATE TABLE `detail_pinjam` (
  `kode_pinjam` int(11) DEFAULT NULL,
  `kode_barang` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `status` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_pinjam` */

insert  into `detail_pinjam`(`kode_pinjam`,`kode_barang`,`jumlah`,`status`) values 
(7,3,5,NULL),
(7,2,3,NULL),
(8,2,10,NULL),
(8,3,5,NULL);

/*Table structure for table `m_barang` */

DROP TABLE IF EXISTS `m_barang`;

CREATE TABLE `m_barang` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(100) DEFAULT NULL,
  `kode_kategori` int(11) DEFAULT NULL,
  `kode_merk` int(11) DEFAULT NULL,
  `stok` int(5) DEFAULT NULL,
  `harga_sewa` int(5) DEFAULT NULL,
  `gambar` text,
  `desc` text,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `m_barang` */

insert  into `m_barang`(`kode`,`nama_barang`,`kode_kategori`,`kode_merk`,`stok`,`harga_sewa`,`gambar`,`desc`) values 
(1,'Tenda 2P',1,1,41,5000,'Tenda_2P.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.'),
(2,'Carrier Eiger',3,2,25,10000,'Carrier_Eiger.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.'),
(3,'Cooking Set Eiger',6,5,25,5000,'Cooking_Set_Eiger.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.'),
(4,'Matras Eiger',4,3,50,5000,'Matras_Eiger.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.'),
(5,'Sepatu Eiger',5,4,50,5000,'Sepatu_Eiger.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.'),
(6,'Sepatu Consina',5,6,50,5000,'Sepatu_Consina.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic tenetur.');

/*Table structure for table `m_denda` */

DROP TABLE IF EXISTS `m_denda`;

CREATE TABLE `m_denda` (
  `kode` int(5) NOT NULL AUTO_INCREMENT,
  `nama_denda` varchar(50) DEFAULT NULL,
  `harga` int(5) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_denda` */

/*Table structure for table `m_kategori` */

DROP TABLE IF EXISTS `m_kategori`;

CREATE TABLE `m_kategori` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `m_kategori` */

insert  into `m_kategori`(`kode`,`nama_kategori`) values 
(1,'Tenda'),
(3,'Carrier'),
(4,'Matrass'),
(5,'Sepatu'),
(6,'Cooking Set');

/*Table structure for table `m_konsumen` */

DROP TABLE IF EXISTS `m_konsumen`;

CREATE TABLE `m_konsumen` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `no_telp` varchar(13) DEFAULT NULL,
  `alamat` text,
  `jk` enum('L','P') DEFAULT NULL,
  `no_identitas` varchar(50) DEFAULT NULL,
  `status` enum('member','nonmember') DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `m_konsumen` */

insert  into `m_konsumen`(`kode`,`nama_lengkap`,`no_telp`,`alamat`,`jk`,`no_identitas`,`status`) values 
(1,'mocang','08196776','mocang','L','33333','member'),
(2,'Ihsan','08136489455','Gang Soma','L','111111','member');

/*Table structure for table `m_merk` */

DROP TABLE IF EXISTS `m_merk`;

CREATE TABLE `m_merk` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merk` varchar(50) DEFAULT NULL,
  `kode_kategori` int(11) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `m_merk` */

insert  into `m_merk`(`kode`,`nama_merk`,`kode_kategori`) values 
(1,'Eiger',1),
(2,'Eiger',3),
(3,'Eiger',4),
(4,'Eiger',5),
(5,'Eiger',6),
(6,'Consina',5);

/*Table structure for table `m_user` */

DROP TABLE IF EXISTS `m_user`;

CREATE TABLE `m_user` (
  `kode` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` int(1) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;

/*Data for the table `m_user` */

insert  into `m_user`(`kode`,`username`,`password`,`role`,`nama_lengkap`) values 
(1,'mocang','0192023a7bbd73250516f069df18b500',4,'mocang'),
(2,'icen','0192023a7bbd73250516f069df18b500',4,'Ihsan'),
(997,'gudang','0192023a7bbd73250516f069df18b500',3,'Gudang'),
(998,'pemilik','0192023a7bbd73250516f069df18b500',2,'Pemilik'),
(999,'admin','0192023a7bbd73250516f069df18b500',1,'Administrator');

/*Table structure for table `t_beli` */

DROP TABLE IF EXISTS `t_beli`;

CREATE TABLE `t_beli` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kategori` int(50) DEFAULT NULL,
  `kode_merk` int(50) DEFAULT NULL,
  `kode_barang` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `t_beli` */

insert  into `t_beli`(`kode`,`kode_kategori`,`kode_merk`,`kode_barang`,`jumlah`,`harga`,`tgl`,`status`) values 
(1,3,3,2,10,5000,'2019-03-14',NULL),
(2,3,3,2,10,100000,'2019-03-14','1'),
(3,1,1,1,5,50000,'2019-03-14','1');

/*Table structure for table `t_kembali` */

DROP TABLE IF EXISTS `t_kembali`;

CREATE TABLE `t_kembali` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_sewa` date DEFAULT NULL,
  `tgl_dibuat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_kembali` date DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_kembali` */

/*Table structure for table `t_pinjam` */

DROP TABLE IF EXISTS `t_pinjam`;

CREATE TABLE `t_pinjam` (
  `kode` int(5) NOT NULL AUTO_INCREMENT,
  `kode_konsumen` int(5) DEFAULT NULL,
  `tgl_sewa` date DEFAULT NULL,
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_kembali` date NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `bayar` float DEFAULT NULL,
  `kembalian` float DEFAULT NULL,
  `denda` float DEFAULT NULL,
  `bayar_denda` float DEFAULT NULL,
  `kembalian_denda` float DEFAULT NULL,
  `ket_denda` text,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `t_pinjam` */

insert  into `t_pinjam`(`kode`,`kode_konsumen`,`tgl_sewa`,`tgl_dibuat`,`tgl_kembali`,`status`,`bayar`,`kembalian`,`denda`,`bayar_denda`,`kembalian_denda`,`ket_denda`) values 
(7,1,'2019-03-01','2019-03-13 10:37:47','2019-03-03','Dipinjamkan',30000,15000,3960000,4000000,40000,NULL),
(8,1,'2019-03-11','2019-03-12 11:35:09','2019-03-12','Dikembalikan',20000,5000,3375000,0,0,NULL);

/* Trigger structure for table `detail_pinjam` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `kurangi_stok` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `kurangi_stok` AFTER INSERT ON `detail_pinjam` FOR EACH ROW BEGIN
UPDATE m_barang Set stok=stok-New.jumlah
WHERE kode=New.kode_barang;
END */$$


DELIMITER ;

/*Table structure for table `vbeli` */

DROP TABLE IF EXISTS `vbeli`;

/*!50001 DROP VIEW IF EXISTS `vbeli` */;
/*!50001 DROP TABLE IF EXISTS `vbeli` */;

/*!50001 CREATE TABLE  `vbeli`(
 `kode` int(11) ,
 `kode_kategori` int(50) ,
 `kode_merk` int(50) ,
 `kode_barang` int(11) ,
 `jumlah` int(11) ,
 `harga` float ,
 `tgl` date ,
 `status` enum('0','1') ,
 `nama_barang` varchar(100) ,
 `nama_merk` varchar(50) ,
 `nama_kategori` varchar(50) 
)*/;

/*Table structure for table `vdetail` */

DROP TABLE IF EXISTS `vdetail`;

/*!50001 DROP VIEW IF EXISTS `vdetail` */;
/*!50001 DROP TABLE IF EXISTS `vdetail` */;

/*!50001 CREATE TABLE  `vdetail`(
 `kode` int(5) ,
 `nama_lengkap` varchar(50) ,
 `no_telp` varchar(13) ,
 `alamat` text ,
 `tgl_sewa` date ,
 `tgl_kembali` date ,
 `nama_barang` varchar(100) ,
 `jumlah` int(11) ,
 `harga_sewa` int(5) ,
 `no_identitas` varchar(50) ,
 `bayar` float ,
 `kembalian` float ,
 `status_pinjam` varchar(50) ,
 `denda` float ,
 `statkonsumen` enum('member','nonmember') ,
 `stat` varchar(11) 
)*/;

/*Table structure for table `vpinjam` */

DROP TABLE IF EXISTS `vpinjam`;

/*!50001 DROP VIEW IF EXISTS `vpinjam` */;
/*!50001 DROP TABLE IF EXISTS `vpinjam` */;

/*!50001 CREATE TABLE  `vpinjam`(
 `kode` int(5) ,
 `nama_lengkap` varchar(50) ,
 `tgl_sewa` date ,
 `tgl_kembali` date ,
 `kode_konsumen` int(5) ,
 `stat` varchar(50) ,
 `status` varchar(11) ,
 `selisih` int(7) 
)*/;

/*View structure for view vbeli */

/*!50001 DROP TABLE IF EXISTS `vbeli` */;
/*!50001 DROP VIEW IF EXISTS `vbeli` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vbeli` AS (select `t_beli`.`kode` AS `kode`,`t_beli`.`kode_kategori` AS `kode_kategori`,`t_beli`.`kode_merk` AS `kode_merk`,`t_beli`.`kode_barang` AS `kode_barang`,`t_beli`.`jumlah` AS `jumlah`,`t_beli`.`harga` AS `harga`,`t_beli`.`tgl` AS `tgl`,`t_beli`.`status` AS `status`,`m_barang`.`nama_barang` AS `nama_barang`,`m_merk`.`nama_merk` AS `nama_merk`,`m_kategori`.`nama_kategori` AS `nama_kategori` from (((`t_beli` join `m_barang` on((`m_barang`.`kode` = `t_beli`.`kode_barang`))) join `m_merk` on((`m_merk`.`kode` = `t_beli`.`kode_merk`))) join `m_kategori` on((`m_kategori`.`kode` = `t_beli`.`kode_kategori`)))) */;

/*View structure for view vdetail */

/*!50001 DROP TABLE IF EXISTS `vdetail` */;
/*!50001 DROP VIEW IF EXISTS `vdetail` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vdetail` AS select `t_pinjam`.`kode` AS `kode`,`m_konsumen`.`nama_lengkap` AS `nama_lengkap`,`m_konsumen`.`no_telp` AS `no_telp`,`m_konsumen`.`alamat` AS `alamat`,`t_pinjam`.`tgl_sewa` AS `tgl_sewa`,`t_pinjam`.`tgl_kembali` AS `tgl_kembali`,`m_barang`.`nama_barang` AS `nama_barang`,`detail_pinjam`.`jumlah` AS `jumlah`,`m_barang`.`harga_sewa` AS `harga_sewa`,`m_konsumen`.`no_identitas` AS `no_identitas`,`t_pinjam`.`bayar` AS `bayar`,`t_pinjam`.`kembalian` AS `kembalian`,`t_pinjam`.`status` AS `status_pinjam`,`t_pinjam`.`denda` AS `denda`,`m_konsumen`.`status` AS `statkonsumen`,if((now() > `t_pinjam`.`tgl_kembali`),'Denda','Tidak Denda') AS `stat` from (((`t_pinjam` join `detail_pinjam` on((`t_pinjam`.`kode` = `detail_pinjam`.`kode_pinjam`))) join `m_barang` on((`m_barang`.`kode` = `detail_pinjam`.`kode_barang`))) join `m_konsumen` on((`m_konsumen`.`kode` = `t_pinjam`.`kode_konsumen`))) */;

/*View structure for view vpinjam */

/*!50001 DROP TABLE IF EXISTS `vpinjam` */;
/*!50001 DROP VIEW IF EXISTS `vpinjam` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vpinjam` AS select `t_pinjam`.`kode` AS `kode`,`m_konsumen`.`nama_lengkap` AS `nama_lengkap`,`t_pinjam`.`tgl_sewa` AS `tgl_sewa`,`t_pinjam`.`tgl_kembali` AS `tgl_kembali`,`t_pinjam`.`kode_konsumen` AS `kode_konsumen`,`t_pinjam`.`status` AS `stat`,if((cast(now() as date) > `t_pinjam`.`tgl_kembali`),'Denda','Tidak Denda') AS `status`,(to_days(now()) - to_days(`t_pinjam`.`tgl_kembali`)) AS `selisih` from (`t_pinjam` join `m_konsumen` on((`m_konsumen`.`kode` = `t_pinjam`.`kode_konsumen`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
