<?php

function fill_kategori()
{
    $output = '';
    $q = $this->Data_model->get_kategori();
    foreach ($q as $row) {
        $output .= '<option value="' . $row->kode . '">' . $row->nama_kategori . '</option>';
    }
    return $output;
}

function fill_merk()
{
    $output = '';
}

function fill_barang()
{
    $output = '';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?=APP_NAME;?>">
        <meta name="keywords" content="HTML,CSS,JavaScript">
        <meta name="author" content="<?=AUTHOR;?>">
        <title><?=APP_NAME;?> | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/font-awesome/css/font-awesome.min.css');?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/Ionicons/css/ionicons.min.css')?>">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/jvectormap/jquery-jvectormap.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css')?>">


        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('assets/admin/dist/css/AdminLTE.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/admin/dist/css/skins/_all-skins.min.css')?>">
        <!-- Google Font -->
        <!-- CUSTOM CSS -->
<?php ($css != '') ? $this->load->view($css) : '';?>
    </head>
    <body class="hold-transition skin-red-light">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?=base_url('dashboard');?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b><img src="<?=base_url('assets/admin/dist/img/avatar5.png')?>" class="user-image"></b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b><img src="<?=base_url('assets/admin/dist/img/avatar5.png')?>" class="user-image" style="height: 50px;width: 154px;"> RENTAL</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?=base_url('assets/admin/dist/img/avatar5.png');?>" class="user-image" alt="<?=(isset($_SESSION['nama_lengkap']) ? $_SESSION['nama_lengkap'] : '');?>">
                                <span class="hidden-xs"><?=(isset($_SESSION['nama']) ? $_SESSION['nama'] : '');?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?=base_url('assets/admin/dist/img/avatar5.png');?>" class="img-circle" alt="<?=(isset($_SESSION['nama_lengkap']) ? $_SESSION['nama_lengkap'] : '');?>">
                                    <p>
                                        <?=(isset($_SESSION['nama_lengkap']) ? $_SESSION['nama_lengkap'] : '');?>
                                        <?php
if ($_SESSION['role'] == 1) {
    $role = "Admin," . $_SESSION['nama_lengkap'];
} else if ($_SESSION['role'] == 2) {
    $role = "Owner," . $_SESSION['nama_lengkap'];
} else {
    $role = "Owner," . $_SESSION['nama_lengkap'];
}
?>
                                        <small><?=$role;?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?=base_url('login/doOut')?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>