<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <!-- ADMIN -->
            <?php if ($_SESSION['role'] == 1): ?>
                <li>
                    <a href="<?=base_url('dashboard');?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?=base_url('master');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>Master Data</span>
                    </a>
                </li>
                <li>
                    <a href="<?=base_url('peminjaman');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>Peminjaman</span>
                    </a>
                </li>
                <li>
                    <a href="<?=base_url('pengembalian');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>List Pengembalian</span>
                    </a>
                </li>
            <?php endif;?>
            <!--PEMILIK-->
            <?php if ($_SESSION['role'] == 2): ?>
                <li>
                    <a href="<?=base_url('pembelian');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>Pembelian</span>
                    </a>
                </li>
                <li>
                    <a href="<?=base_url('penerimaan');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>Penerimaan</span>
                    </a>
                </li>
                <li>
                    <a href="<?=base_url('laporan');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>Laporan</span>
                    </a>
                </li>
            <?php endif;?>
            <!--GUDANG-->
            <?php if ($_SESSION['role'] == 3): ?>
                <li>
                    <a href="<?=base_url('pengembalian');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>List Pengembalian</span>
                    </a>
                </li>
            <?php endif;?>
            <!--MEMBER-->
            <?php if ($_SESSION['role'] == 4): ?>
            <li>
                <a href="<?=base_url('dashboard');?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
                <li>
                    <a href="<?=base_url('peminjaman');?>" class="">
                        <i class="fa fa-database"></i>
                        <span>Peminjaman</span>
                    </a>
                </li>
            <?php endif;?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>