
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?= date('Y') ?> <a href="#"><?= AUTHOR; ?></a>.</strong> All rights
    reserved.
</footer>

<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?= base_url('assets/admin/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/admin/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/admin/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/admin/dist/js/adminlte.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?= base_url('assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap  -->
<script src="<?= base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?= base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- BOOTSTRAP GROWL -->
<script src="<?= base_url('assets/admin/bower_components/bootstrap-growl/bootstrap-growl.min.js') ?>"></script>
<!-- MY CUSTOM JS -->
<script src="<?= base_url('assets/js/functions.js') ?>" type="text/javascript"></script>  
<script src="<?= base_url('assets/dataTables.bootstrap4.min.js') ?>"></script>
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/admin/bower_components/select2/dist/css/select2.min.css'); ?>">
<!-- Select2 -->
<script src="<?= base_url('assets/admin/bower_components/select2/dist/js/select2.full.min.js'); ?>"></script>
<?php ($js != '') ? $this->load->view($js) : ''; ?>
</body>
</html>
