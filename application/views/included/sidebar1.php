<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?=base_url('home/profile');?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Profile</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url('home');?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Barang</span>
                </a>
            </li>
            <li>
                <a href="<?=base_url('home/carapemesanan');?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Cara Pemesanan</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>