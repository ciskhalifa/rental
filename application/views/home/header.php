<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Fusion - Bootstrap 4 Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/css/owl.theme.css">
    <!-- Animate -->
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="<?=base_url('assets/users')?>/css/responsive.css">
    <style>
.filterDiv {

  display: none;
}

.show {
  display: block;
}

/* Style the buttons */

.btn:hover {
  background-color: #ddd;
}

.btn.active {
  background-color: #666;
  color: white;
}
</style>
  </head>
  <body>
      <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a href="#" class="navbar-brand"><img src="<?=base_url('assets/users')?>/img/logo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
                <a class="nav-link" href="#hero-area">
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#team">
                  Product
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Navbar End -->

      <!-- Hero Area Start -->
      <div id="hero-area" class="hero-area-bg">
        <div class="container">
          <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
              <div class="contents">
                <h2 class="head-title">App, Business & SaaS<br>Landing Page Template</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem repellendus quasi fuga nesciunt dolorum nulla magnam veniam sapiente, fugiat! fuga nesciunt dolorum nulla magnam veniam sapiente, fugiat!</p>
                <div class="header-button">
                  <a href="<?=base_url('register');?>" class="btn btn-common" target="_BLANK">Register</i></a>
                  <a href="<?=base_url('member');?>" class="btn btn-border video-popup" target="_BLANK">Login</i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
              <div class="intro-img">
                <img class="img-fluid" src="<?=base_url('assets/users')?>/img/intro-mobile.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Hero Area End -->

    </header>
    <!-- Header Area wrapper End -->
