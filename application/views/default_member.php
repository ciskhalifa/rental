<?php
    $data['js'] = $js;
    $data['css'] = $css;

    $this->load->view(VIEW_INCLUDE . '/header1', $data);
    $this->load->view(VIEW_INCLUDE . '/sidebar1');
    echo '<div class="content-wrapper">';
    $this->load->view($content);
    echo '</div>';
    $this->load->view(VIEW_INCLUDE . '/footer1', $data);
    
?>