<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load cart library
        $this->load->library('cart');

        // Load product model
        //$this->load->model('product');
    }

    public function index()
    {
        $data = array();
        $data['content'] = 'index';
        $data['css'] = 'css';
        $data['js'] = 'js';
        // Retrieve cart data from the session
        $data['cartItems'] = $this->cart->contents();

        // Load the cart view
        $this->load->view('default', $data);
    }

    public function updateItemQty()
    {
        $update = 0;
        // print_r($this->input->get());
        // die;
        // Get cart item info
        $rowid = $this->input->get('rowid');
        $qty = $this->input->get('qty');

        // Update item in the cart
        if (!empty($rowid) && !empty($qty)) {
            $data = array(
                'rowid' => $rowid,
                'qty' => $qty,
            );
            $update = $this->cart->update($data);
        }
        // Return response
        echo $update ? 'ok' : 'err';
    }
    public function addToCart($proID)
    {
        $proID = $this->uri->segment(3);
        // Fetch specific product by ID
        $product = $this->Data_model->satuData('m_barang', array('kode' => $proID));

        // Add product to the cart
        $data = array(
            'id' => $product->kode,
            'qty' => 1,
            'price' => $product->harga_sewa,
            'name' => $product->nama_barang,
            'gambar' => $product->gambar,
        );
        print_r($data);
        $this->cart->insert($data);

        // Redirect to the cart page
        redirect('cart');
    }
    public function removeItem($rowid)
    {
        // Remove item from cart
        $remove = $this->cart->remove($rowid);

        // Redirect to the cart page
        redirect('cart');
    }

    public function checkout()
    {
        if ($this->cart->total_items() <= 0) {
            redirect('dashboard');
        }
        $custData = $data = array();
        $submit = $this->input->post('placeOrder');
    }

}
