<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Member extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
       
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'member_login';

        $this->load->view('layout_login_member', $data);
    }

    public function doLogin() {
        if (IS_AJAX) {
            $res = $this->Data_model->verify_user(array('username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))), 'm_user');
            if ($res !== FALSE) {
                foreach ($res as $row => $kolom) {
                    $_SESSION[$row] = $kolom;
                }
                echo base_url('dashboard');
            } else {
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button> Password / Username tidak ditemukan/salah.';
            }
        }
    }

    public function doOut() {
        session_unset();
        session_destroy();
        $this->index();
    }

}
