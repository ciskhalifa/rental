<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Penerimaan</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <table id="data-penerimaan" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php
if ($kolom) {
    foreach ($kolom as $key => $value) {
        if (strlen($value) == 0) {
            echo '<th data-type="numeric"></th>';
        } else {
            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
        }
    }
}
?>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
$no = 1;
foreach ($rowdata as $row):
?>
                                <tr>
                                    <td><?="TRX-" . $row->kode;?></td>
                                    <td><?=$row->nama_kategori;?></td>
                                    <td><?=$row->nama_merk;?></td>
                                    <td><?=$row->nama_barang;?></td>
                                    <td><?=$row->jumlah;?></td>
                                    <td><?=$row->harga;?></td>
                                    <td><?=$row->tgl;?></td>
                                    <td><?="Rp. " . number_format($row->jumlah * $row->harga, 2, ',', '.');?></td>
                                    <td>
                                        <button id="btnUpdate" class="btn btn-success btn-md btnUpdate"  data-mtabel="t_beli" data-get="penerimaan/updatestok" data-id="<?=$row->kode;?>" data-default="<?=$row->kode_barang;?>"><i class="fa fa-refresh"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="myConfirm" class="modal fade">
    <div class="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">

                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-success">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>