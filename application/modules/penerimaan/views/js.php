<script src="<?=base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script>
    $(function () {
        $('#data-penerimaan').DataTable();
        $('.btnUpdate').bind('click', function () {
            $('#getto').val($(this).attr("data-get"));
            $('#aepYes').text('Update Stok');
            $("#cid").val($(this).attr("data-id"));
            $("#cod").val($(this).attr("data-mtabel"));
            $('.lblModal').text('Update Status Pada Data ' + $(this).attr("data-default"));
            $('#myConfirm').modal();
        });
        
        $('.btnStat2').bind('click', function () {
            $("#cid").val($(this).attr("data-id"));
            var link = 'pengembalian/cekdata';
            $.ajax({
                url: link, type: "POST", data: "cid=" + $("#cid").val(), dataType: "html", beforeSend: function() {
                    if (link != "#") {

                    }
                },
                success: function(html) {
                    $(".denda").val(html);
                }
            });
            $('#myConfirm2').modal();
            $('#getto').val($(this).attr("data-get"));
            $('#aepYes').text('Hapus');
            $("#cid").val($(this).attr("data-id"));
            $("#cod").val($(this).attr("data-mtabel"));
            // $('.lblModal').text('Update Status Pada sss ' + $(this).attr("data-default"));
            // $('#myConfirm2').modal();
        });
        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=pinjam", dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    notify("Berhasil diupdate", "success")
                    $("#myConfirm").modal("hide")
                    location.reload(true);
                }})
        });
    });
</script>