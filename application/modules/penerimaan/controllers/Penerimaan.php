<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaan extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        $data['content'] = 'penerimaan';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("Kode Pembelian", "Kategori", "Merk", "Barang", "Jumlah", "Harga", "Tanggal Beli", "Total", "Opsi");
        $sTable = 'vbeli';
        $sWhere = "";
        $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE status= '0' $sWhere ";
        $data['rowdata'] = $this->Data_model->jalankanQuery($tQuery, 3);
        $this->load->view('default', $data);
    }

    public function updateStok()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            $q = $this->Data_model->jalankanQuery("SELECT * FROM t_beli WHERE kode=" . $param, 3);

            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $arrdata = array('status' => '1');

                foreach ($q as $row) {
                    $q = $this->Data_model->jalankanQuery("SELECT * FROM m_barang WHERE kode=" . $row->kode_barang, 3);
                    foreach ($q as $data) {
                        $kode[] = $data->kode;
                        $stok[] = $data->stok + $row->jumlah;
                    }
                }
                for ($i = 0; $i < count($stok); $i++) {
                    $this->Data_model->updateDataWhere(array('stok' => $stok[$i]), 'm_barang', array('kode' => $kode[$i]));
                }
                $this->Data_model->updateDataWhere($arrdata, 't_beli', $kondisi);

                echo json_encode("ok");
            }
        }
    }

}
