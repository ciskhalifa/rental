<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Home extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['products'] = $this->Data_model->jalankanQuery("SELECT * FROM m_barang", 3);
        $data['content'] = 'index';
        $data['css'] = 'css';
        $data['js'] = 'js';

        $this->load->view('default_member', $data);
    }

    public function profile()
    {
        $data['content'] = 'profile';
        $data['css'] = 'css';
        $data['js'] = 'js';

        $this->load->view('default_member', $data);
    }

    public function carapemesanan()
    {
        $data['content'] = 'carapemesanan';
        $data['css'] = 'css';
        $data['js'] = 'js';

        $this->load->view('default_member', $data);
    }
}
