<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cara Pemesanan</h3>
                    <div class="box-tools pull-right">
                        
                    </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-8">
                    Syarat & Ketentuan
TATA TERTIB PERSEWAAN<br>
<br>
• Penyewa Wajib mengambil barang sesuai dengan tanggal pemesanan dan mengembalikan barang sesuai dengan tanggal pengembalian.<br>
•Penyewa Wajib Mengecek semua perlengkapan yang disewa sebelum meninggalkan Lokasi persewaan Anak Rimba Adventure. Kami tidak menerima komplain dalam bentuk apapun apabila Penyewa telah meninggalkan lokasi Persewaan.<br>
•Peralatan/Perlengkapan persewaan dalam kondisi baik dan utuh, Penyewa Wajib merawat dan menjaganya dengan baik.<br>
•Jauhkan barang dari api, kecuali kompor dan nisting.<br>
•Dilarang Merokok dan Memasak di dalam Tenda.<br>
•Dilarang mendirikan tenda terlalu dekat api unggun.<br>
•Dilarang Mendirikan tenda di lahan yang berbatu dan terdapat benda tajam lainya (patahan ranting, pecahan batu tajam, dll).<br>
•Membawa barang dengan baik dan benar.<br>
Dll.

Jam Kerja kami adalah Pukul 08.00 – 21.00 WIB. (ON TIME) Jika terjadi keterlambatan pengembalian, maka di anggap memperpanjang sewa (tidak ada toleransi waktu lagi, jam 21.00 tepat kami TUTUP).<br>
Saat Ibadah Shalat, Toko TUTUP / ISTIRAHAT sebentar. Harap Semua Pelanggan Bisa Menyesuaikan dan Menghormati.<br>
Khusus hari Jum’at pukul 11.30 – 13.00 WIB TUTUP untuk melaksanakan IBADAH JUM’AT.<br>
Di luar jam kerja tidak bisa kami layani.<br>
Telpon, SMS, BBM, WA hanya untuk menanyakan stok barang dan informasi barang. Boking atau Pemesanan atau Pengubahan Pesanan pada Nota harus langsung Ke Basecamp dan membawa Nota Asli.<br>
Toko Anak Rimba Adventure bukanlah tempat penitipan barang. Pelanggan di larang menitipkan barang bawaan apapun.<br>
Toko Anak Rimba Adventure Setiap Hari BUKA. Libur hanya pada saat Hari Raya Besar ISLAM dan Libur Bersama Hari raya islam (Idul Fitri dan Idul Adha). Libur atau TUTUP Juga bisa terjadi Karena adanya peristiwa/agenda yang bersifat Darurat atau Penting*
Apabila terjadi kerusakan atau hilang terhadap peralatan yang di sewa, penyewa bertanggung jawab sepenuhnya untuk memperbaiki atau mengganti peralatan yang sama/serupa. (untuk keterangan denda dan kerusakan bisa dilihat di KITAB UNDANG-UNDANG PERSEWAAN KERUSAKAN/DENDA).
Persiapkanlah kegiatan anda dengan baik (Manajemen waktu, manajemen Perjalanan dan manajemen perlengkapan) karena kegiatan outdoor adalah kegiatan yang berisiko tinggi dan susah di prediksi.
Ketentuan tersebut di atas dapat berubah sesuai dengan situasi dan kondisi***.
Point-point tersebut di atas merupakan kesepakatan yang harus ditaati bagi penyewa.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
