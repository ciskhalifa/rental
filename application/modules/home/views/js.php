
<script src="<?= base_url('assets/js/jquery.isloading.min.js'); ?>"></script> 
<script src="<?= base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script>
    var myApp = myApp || {};
    $(function () {

    });
</script>

<script>
    $(function () {
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: '<?= base_url('login/doLoginMember') ?>',
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {},
                    success: function (d) {
                        if (d.substring(0, 4) == "http") {
                            window.location.href = d
                        } else {
                            $(".alert").html(d);
                            $(".alert").slideDown()
                        }
                        setTimeout(function () {}, 2000)
                    },
                    error: function () {}
                });
                return false
            }
            return false
        })
    });
</script>
