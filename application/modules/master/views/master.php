<section class="content">
    <div class="row">
        <div class="col-md-2">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Master Data</h3>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <?php if ($_SESSION['role'] == 1): ?>
                            <li class="menuitem" data-default="barang"><a href="javascript:;"><i class="fa fa-cubes"></i> Barang</a></li>
                            <li class="menuitem" data-default="kategori"><a href="javascript:;"><i class="fa fa-cubes"></i> Kategori Barang</a></li>
                            <li class="menuitem" data-default="merk"><a href="javascript:;"><i class="fa fa-cubes"></i> Merk Barang</a></li>
                            <li class="menuitem" data-default="konsumen"><a href="javascript:;"><i class="fa fa-users"></i> Konsumen</a></li>
                            <li class="menuitem" data-default="user"><a href="javascript:;"><i class="fa fa-user"></i> Users</a></li>                           
                        <?php elseif ($_SESSION['role'] == 2): ?>
                            <li class="menuitem" data-default="barang"><a href="javascript:;"><i class="fa fa-cubes"></i> Barang</a></li>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
        </div>
        <div id="divhalaman"></div>
    </div>

</section>

