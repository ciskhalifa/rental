<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $nama_merk = $rowdata->nama_merk;
} else {
    $cid = "";
    $nama_merk = "";
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group">
            <label class="col-md-2 label-control">Kategori</label>
            <div class="col-md-4">
                <select name="kode_kategori" class="form-control select2">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['kode_kategori'] : '';
                    $q = $this->Data_model->selectData("m_kategori", "kode");
                    foreach ($q as $row):
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        ?>
                        <option value="<?= $row->kode; ?>" <?= $kapilih; ?>><?= $row->nama_kategori; ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Nama Merk</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama Merk" name="nama_merk"  value="<?php echo $nama_merk; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function () {
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>