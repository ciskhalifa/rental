<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $kode = $rowdata->kode;
    $username = $rowdata->username;
    $password = $rowdata->password;
    $role = $rowdata->role;
    $nama_lengkap = $rowdata->nama_lengkap;
} else {
    $cid = "";
    $kode = "";
    $username = "";
    $password = "";
    $role = "";
    $nama_lengkap = "";
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Nama Lengkap</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama Lengkap" name="nama_lengkap"  value="<?php echo $nama_lengkap; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Username</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Username" name="username" id="username" value="<?php echo $username; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Password</label>
            <div class="col-md-4">
                <input type="password" class="form-control input-sm" placeholder="Password" name="password" id="password" value="<?php echo $password; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Role</label>
            <div class="col-md-4">
                <select class="select2 form-control" name="role" id="role">
                    <option value="">- Pilihan -</option>
                    <option value="1" <?= ($role == 1) ? " selected= selected" : "" ?>> Admin </option>
                    <option value="2" <?= ($role == 2) ? " selected= selected" : "" ?>> Operasional </option>                    
                    <option value="3" <?= ($role == 3) ? " selected= selected" : "" ?>> Pemilik </option>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function () {
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>