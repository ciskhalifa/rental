<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $nama_barang = $rowdata->nama_barang;
    $stok = $rowdata->stok;
    $harga_sewa = $rowdata->harga_sewa;
    $kode_kategori = $rowdata->kode_kategori;
    $kode_merk = $rowdata->kode_merk;
    $gambar = $rowdata->gambar;
    $desc = $rowdata->desc;
} else {
    $cid = "";
    $nama_barang = "";
    $stok = "";
    $harga_sewa = "";
    $kode_kategori = "";
    $kode_merk = "";
    $gambar = "";
    $desc = "";
}
?>

<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal" method="POST">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group">
            <label class="col-md-2 label-control">Kategori</label>
            <div class="col-md-4">
                <select name="kode_kategori" class="form-control select2" id="kategori">
                    <option value="">- Pilihan -</option>
                    <?php
$n = ($kode_kategori != "") ? $kode_kategori : '';
$q = $this->Data_model->get_kategori();
foreach ($q as $row):
    $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
    ?>
																		                        <option value="<?=$row->kode;?>" <?=$kapilih;?>><?=$row->nama_kategori;?></option>
																		                    <?php endforeach;?>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 label-control">Merk</label>
            <div class="col-md-4">
                <select name="kode_merk" class="form-control select2" id="merk">
                    <option value="">- Pilihan -</option>
                    <?php
$n = ($kode_merk != "") ? $kode_merk : '';
$q = $this->Data_model->get_merk();
foreach ($q as $row):
    $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
    ?>
																		                        <option class="<?=$row->kodekat;?>" value="<?=$row->kode;?>" <?=$kapilih;?>><?=$row->nama_merk;?></option>
																		                    <?php endforeach;?>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Nama Barang</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama Barang" name="nama_barang" id="nama_barang" value="<?php echo $nama_barang; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Stok</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Stok" name="stok" id="username" value="<?php echo $stok; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Harga Sewa</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Harga Sewa" name="harga_sewa" id="username" value="<?php echo $harga_sewa; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Description</label>
            <div class="col-md-4">
                <textarea class="form-control" name="desc"><?=($desc == "") ? "" : $desc?></textarea>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
        <label class="col-md-2 label-control">Foto</label>
        <div class="col-md-2">
        <input type="file" class="form-control" id="file" name="gambar" required />
        </div>
    </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script src="<?=base_url('assets/jquery.chained.min.js')?>"></script>
<script>
    $(function () {
        $("#file").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
            alert('Please select a valid image file (JPEG/JPG/PNG).');
            $("#file").val('');
            return false;
            }
        });
        // disini kita hubungkan kota dengan provinsi
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = new FormData($('#xfrm')[0]);
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //dataType: "html",
                    beforeSend: function () {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
        $("#merk").chained("#kategori");
    }); /*]]>*/
</script>