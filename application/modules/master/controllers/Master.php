<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Master extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        // halaman
        $data['content'] = 'master';
        // js tambahan dan css
        $data['js'] = 'js';
        $data['css'] = 'css';

        $this->load->view('default', $data);
    }

    public function loadHalaman()
    {
        $konten['tabel'] = $this->uri->segment(3); // barang
        switch ($this->uri->segment(3)):
    case 'barang':
        $konten['kolom'] = array("Kode", "Nama Barang", "Kategori", "Merk", "Stok", "Harga Sewa", "Opsi");
        break;
    case 'denda':
        $konten['kolom'] = array("Kode", "Nama Denda", "Harga", "Opsi");
        break;
    case 'konsumen':
        $konten['kolom'] = array("Kode", "Nama Lengkap", "No. Telp", "Alamat", "Opsi");
        break;
    case 'user':
        $konten['kolom'] = array("Kode", "Nama Lengkap", "Username", "Role", "Opsi");
        break;
    case 'kategori':
        $konten['kolom'] = array("Kode", "Nama Kategori", "Opsi");
        break;
    case 'merk':
        $konten['kolom'] = array("Kode", "Nama Merk", "Opsi");
        break;
    default:
        $konten['kolom'] = "";
        break;
        endswitch;

        $konten['jmlkolom'] = count($konten['kolom']);
        $this->load->view('halaman', $konten);
    }

    public function getData()
    {
            /*
             * list data
             */
            if (IS_AJAX) {
                $sTablex = "";
                $order = "";
                $sTable = 'm_' . $this->uri->segment(3); // m_barang
                $k = '';
                switch ($this->uri->segment(3)):
            case 'barang':
                $aColumns = array("kode", "nama_barang", "nama_kategori", "nama_merk", "stok", "harga_sewa", "opsi");
                $kolom = "a.kode,a.nama_barang,b.nama_kategori,c.nama_merk,a.stok,a.harga_sewa";
                $where = " 1=1";
                $sIndexColumn = "kode";
                break;
            case 'denda':
                $aColumns = array("kode", "nama_denda", "harga", "opsi");
                $kolom = "kode,nama_denda,harga";
                $where = " 1=1";
                $sIndexColumn = "kode";
                break;
            case 'konsumen':
                $aColumns = array("kode", "nama_lengkap", "no_telp", "alamat", "opsi");
                $kolom = "kode,nama_lengkap,no_telp,alamat";
                $where = " 1=1";
                $sIndexColumn = "kode";
                break;
            case 'user':
                $aColumns = array("kode", "nama_lengkap", "username", "role", "opsi");
                $kolom = "kode, nama_lengkap, username,CASE WHEN role = '1' THEN 'Admin' WHEN '2' THEN 'Operasional' WHEN '3' THEN 'PEMILIK' WHEN '4' THEN 'MEMBER' ELSE '??' END  AS role";
                $where = " role = 1 || role=2 || role=3";
                $sIndexColumn = "kode";
                break;
            case 'kategori':
                $aColumns = array("kode", "nama_kategori", "opsi");
                $kolom = "kode, nama_kategori";
                $where = " 1=1";
                $sIndexColumn = "kode";
                break;
            case 'merk':
                $aColumns = array("kode", "nama_merk", "opsi");
                $kolom = "kode, nama_merk";
                $where = " 1=1";
                $sIndexColumn = "kode";
                break;
            default:

                break;
                endswitch;
                if (isset($kolom) && strlen($kolom) > 0) {
                        //$where = "";
                        if ($this->uri->segment(3) == "barang") {
                            $tQuery = "SELECT $kolom, '$k' AS opsi "
                                . "FROM $sTable a JOIN m_kategori as b ON b.kode = a.kode_kategori JOIN m_merk as c ON c.kode = a.kode_merk WHERE $where";
                            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                        } else {
                            $tQuery = "SELECT $kolom ,'$k' AS opsi  "
                                . "FROM $sTable a $sTablex WHERE $where ";
                            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                        }
                } else {
                    echo "";
                }
            }
    }

    public function loadForm()
    {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) != '-') {
            $nilai = $this->uri->segment(4);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                if ($this->uri->segment(6) == '' || strlen(trim($this->uri->segment(6))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(6)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            } else {
                if ($this->uri->segment(5) == '' || strlen(trim($this->uri->segment(5))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(5)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            }
            $konten['rowdata'] = $this->Data_model->satuData($tabel, $kondisi);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                $konten['jenis'] = ($this->uri->segment(6) == '') ? 'A' : strtoupper($this->uri->segment(6));
            } else {
                $konten['jenis'] = strtoupper($this->uri->segment(5));
            }
        } else {
            $konten['jenis'] = strtoupper($this->uri->segment(5));
        }
        $this->load->view('form/' . $this->uri->segment(3), $konten);
    }

    public function simpanData()
    {
        $arrdata = array();
        $cid = '';
        $tabel = 'm_' . $this->uri->segment(3);
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {

            } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                if ($key == 'cid') {
                    $cid = $value;
                } else {
                    if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("/", $value);
                            $newtgl = $tgl[1] . "/" . $tgl[0] . "/" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                    } else {
                        if ($this->uri->segment(3) == "user") {
                            $arrdata['password'] = md5($this->input->post('password'));
                            $arrdata[$key] = $value;
                        }
                        $arrdata[$key] = $value;
                    }
                }
            }
        }
        /* foto */
        if (strlen(trim($_FILES['gambar']['name'])) > 0) {

            $files = $_FILES;
            $extenstion = pathinfo($_FILES['gambar']['name']);
            $ext = $extenstion['extension'];

            $_FILES['gambar']['name'] = $arrdata['nama_barang'] . "." . $ext;
            $_FILES['gambar']['type'] = $files['gambar']['type'];
            $_FILES['gambar']['tmp_name'] = $files['gambar']['tmp_name'];
            $_FILES['gambar']['error'] = $files['gambar']['error'];
            $_FILES['gambar']['size'] = $files['gambar']['size'];

            if (strlen(trim($_FILES['gambar']['name'])) > 0) {

                $basepath = BASEPATH;
                $stringreplace = str_replace("system", "publik", $basepath);
                $config['upload_path'] = $stringreplace . 'barang';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('gambar')) {
                    // echo 'masuk pa eko';
                    // die;
                    $configs = array(
                        'source_image' => $config['upload_path'] . '/' . $arrdata['nama_barang'] . "." . $ext,
                        'new_image' => $config['upload_path'] . '/thumb',
                        'maintain_ration' => true,
                        'width' => 200,
                        'height' => 200,
                    );
                    $arrdata['gambar'] = str_replace(" ", "_", $arrdata['nama_barang']) . "." . $ext;
                } else {
                    echo $this->upload->display_errors();
                }
            }
        } // nepi dieu
        // print_r($arrdata);
        // die;
        if ($cid == "") {
            try {
                $this->Data_model->simpanData($arrdata, $tabel);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            $kondisi = 'kode';
            try {
                $kondisi = array('kode' => $cid);
                echo $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('m_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }

}
