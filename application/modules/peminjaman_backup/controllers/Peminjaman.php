<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends MX_Controller {

    public function __construct() {
        parent::__construct();
        /*
         * pengecekan session user login, jika belum login maka arahkan ke halaman login         
         */
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index() {

        $data['js'] = "js";
        $data['css'] = "css";
        $data['content'] = "pinjam";
        $data['tabel'] = 'vpinjam';
        $data['kolom'] = array("No", "Kode Pinjam", "Nama Lengkap", "Tanggal Sewa", "Tanggal Kembali", "");
        $data['jmlkolom'] = count($data['kolom']);

        /*
         * Memanggil role untuk menu aktif
         */
        $this->load->view('layout', $data);
    }

    public function listData() {
        if (IS_AJAX) {
            $aColumns = array("no", "kode", "nama_lengkap", "tgl_sewa", "tgl_kembali", "kode");
            $sIndexColumn = "kode";
            $sTable = 'v' . $this->uri->segment(3);
            $sTablex = '';
            $sWhere = "";
            $sGroup = "";
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere  $sGroup";

            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
        }
    }

    public function form() {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) <> '') {
            $halaman = $this->uri->segment(3);
            $nilai = str_replace('_', '/', $this->uri->segment(4));
            $kondisi = 'nip';
            $konten['rowdata'] = $this->Data_model->satuData('t_' . $this->uri->segment(3), array($kondisi => $nilai));
            $konten['rowpendidikan'] = $this->Data_model->selectData('t_pendidikan', 'kode', array($kondisi => $nilai));
            $konten['rowkerja'] = $this->Data_model->selectData('t_kerja', 'kode', array($kondisi => $nilai));
            $konten['rowaplikasi'] = $this->Data_model->selectData('t_aplikasi', 'kode', array($kondisi => $nilai));

//            echo $this->db->last_query();
        } else {
            $halaman = $this->uri->segment(3);
        }
        $this->load->view('form_' . $halaman, $konten);
    }

    public function select_aplikasi() {
        $query = "SELECT kode, nama_aplikasi AS disp FROM m_aplikasi";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }

    public function cetak() {
        $this->load->library('pdf');

        date_default_timezone_set("Asia/Bangkok");
        $data = $this->Data_model->jalankanQuery("select * from vpegawai WHERE nip =" . $this->uri->segment(3), 3);
        $rowpen = $this->Data_model->jalankanQuery("select t_pendidikan.*, m_pendidikan.nama_pendidikan from t_pendidikan JOIN m_pendidikan ON m_pendidikan.kode = t_pendidikan.id_pendidikan WHERE nip =" . $this->uri->segment(3), 3);
        $rowkerja = $this->Data_model->jalankanQuery("select * from t_kerja WHERE nip=" . $this->uri->segment(3), 3);
        $rowapp = $this->Data_model->jalankanQuery("select t_aplikasi.*, m_aplikasi.nama_aplikasi from t_aplikasi JOIN m_aplikasi ON m_aplikasi.kode = t_aplikasi.id_aplikasi WHERE nip =" . $this->uri->segment(3), 3);

        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage('P', 'A4', '');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string 
        $pdf->SetTitle("Data Diri " . $data[0]->nama_lengkap);
        $pdf->Cell(200, 7, 'Data Mitra OS ROC 3', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Ln();
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(55, 6, 'No. KTP', 0, 0);
        $pdf->Cell(50, 6, $data[0]->ktp, 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Nama', 0, 0);
        $pdf->Cell(50, 6, $data[0]->nama_lengkap, 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Jenis Kelamin', 0, 0);
        $pdf->Cell(50, 6, ($data[0]->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan', 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Agama', 0, 0);
        $pdf->Cell(50, 6, $data[0]->nama_agama, 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Alamat Lengkap', 0, 0);
        $pdf->MultiCell(140, 6, $data[0]->alamat, 0, 'L');
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Tempat/Tgl Lahir', 0, 0);
        $pdf->Cell(140, 6, strtoupper($data[0]->tmp_lahir) . ', ' . $this->libglobal->date2ind($data[0]->tgl_lahir), 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Nomor Telepon', 0, 0);
        $pdf->Cell(50, 6, $data[0]->no_hp, 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Email', 0, 0);
        $pdf->Cell(140, 6, $data[0]->email, 0, 0);
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Tanggal Masuk', 0, 0);
        $pdf->Cell(140, 6, ($data[0]->tgl_masuk == NULL) ? '-' : $this->libglobal->date2ind($data[0]->tgl_masuk, 0, 0));
        $pdf->Ln();
        $pdf->Cell(55, 6, 'Unit Kerja', 0, 0);
        $pdf->Cell(50, 6, $data[0]->nama_unit, 0, 0);
        $pdf->Ln();

        if (!empty($rowpen)) {
            // ROW PENDIDIKAN
            //$pdf->Cell(200, 7, 'Data Pendidikan', 0, 1, 'L');
            #buat header tabel
            $pdf->Cell(10, 4, '', 0, 1);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(70, 6, 'Nama Pendidikan', 'TB', 0);
            $pdf->Cell(85, 6, 'Nama Sekolah', 'TB', 0);
            $pdf->Cell(27, 6, 'Tahun Lulus', 'TB', 1, 'C');
            $pdf->SetFont('Arial', '', 10);
            foreach ($rowpen as $row) {
                $pdf->Cell(70, 6, $row->nama_pendidikan, 0, 0);
                $pdf->Cell(85, 6, $row->nama_sekolah, 0, 0);
                $pdf->Cell(27, 6, $row->tahun_lulus, 0, 1, 'C');
            }
            $pdf->Ln();
        }
        if (!empty($rowkerja)) {
            // ROW PENGALAMAN KERJA
            //$pdf->Cell(200, 7, 'Data Pengalaman Kerja', 0, 1, 'L');
            #buat header tabel
            $pdf->Cell(10, 4, '', 0, 1);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(70, 6, 'Nama Perusahaan', 'TB', 0);
            $pdf->Cell(85, 6, 'Posisi', 'TB', 0);
            $pdf->Cell(27, 6, 'Lama Kerja', 'TB', 1, 'C');
            $pdf->SetFont('Arial', '', 10);
            foreach ($rowkerja as $row) {
                $pdf->Cell(70, 6, $row->nama_perusahaan, 0, 0);
                $pdf->Cell(85, 6, $row->posisi, 0, 0);
                $pdf->Cell(27, 6, $row->lama_kerja, 0, 1, 'C');
            }
            $pdf->Ln();
        }
        if (!empty($rowapp)) {
            // ROW APLIKASI
            //$pdf->Cell(200, 7, 'Data Aplikasi', 0, 1, 'L');
            #buat header tabel
            $pdf->Cell(10, 4, '', 0, 1);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(70, 6, 'Nama Aplikasi', 'TB', 0);
            $pdf->Cell(112, 6, 'Username', 'TB', 1);
            $pdf->SetFont('Arial', '', 10);
            foreach ($rowapp as $row) {
                $pdf->Cell(70, 6, $row->nama_aplikasi, 0, 0);
                $pdf->Cell(112, 6, $row->username, 0, 1);
            }
            $pdf->Ln();
        }
        $pdf->Footerttd($data[0]->nama_lengkap);

        $pdf->Output();
    }

    public function cetakresign() {
        $this->load->library('libglobal');
        $data['rowpeg'] = $this->Data_model->jalankanQuery("select * from vpegawai WHERE nip =" . $this->uri->segment(3), 3);
        $data['rowapp'] = $this->Data_model->jalankanQuery("select t_aplikasi.*, m_aplikasi.nama_aplikasi from t_aplikasi JOIN m_aplikasi ON m_aplikasi.kode = t_aplikasi.id_aplikasi WHERE nip =" . $this->uri->segment(3), 3);
        $this->load->view('view_resign', $data);
    }

    function simpanData() {

        $arrdata = array();
        $arrpendidikan = array();
        $arrkerja = array();
        $arraplikasi = array();
        $datauser = array();
        $cid = '';
//        print_r($_FILES);
//        die;
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {
                
            } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                switch (trim($key)):
                    case 'cid':
                        $cid = $value;
                        break;
                    case 'id_pendidikan':
                    case 'tahun_lulus':
                    case 'nama_sekolah':

                        break;
                    case 'nama_perusahaan':
                    case 'posisi':
                    case 'lama_kerja':
                    case 'foto':
                    case 'surat_kontrak':

                        break;
                    case 'id_aplikasi':
                    case 'username':

                        break;
                    default:
                        if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                            if (strlen(trim($value)) > 0) {
                                $tgl = explode("-", $value);
                                $newtgl = $tgl[1] . "-" . $tgl[0] . "-" . $tgl[2];
                                $time = strtotime($newtgl);
                                $arrdata[$key] = $value; // date('Y-m-d', $time);
                            } else {
                                $arrdata[$key] = NULL;
                            }
                        } else {
                            $arrdata[$key] = $value;
                        }
                        break;
                endswitch;
            }
        }

        if ($cid == "") {
            $datauser['nip'] = $this->input->post('nip');
            $datauser['role'] = 2;
            $datauser['username'] = $datauser['nip'];
            $datauser['password'] = md5("telkomjuara");
            $this->Data_model->simpanData($datauser, 'm_user');
            if (is_array($this->input->post('id_pendidikan'))) {
                for ($i = 0; $i < count($this->input->post('id_pendidikan')); $i++) {
                    $arrpendidikan['nip'] = $this->input->post('nip');
                    $arrpendidikan['id_pendidikan'] = $this->input->post('id_pendidikan')[$i];
                    $arrpendidikan['tahun_lulus'] = $this->input->post('tahun_lulus')[$i];
                    $arrpendidikan['nama_sekolah'] = $this->input->post('nama_sekolah')[$i];
                    $this->Data_model->simpanData($arrpendidikan, 't_pendidikan');
                }
            } else {
                $arrpendidikan['nip'] = $this->input->post('nip');
                $arrpendidikan['id_pendidikan'] = $this->input->post('id_pendidikan')[0];
                $arrpendidikan['tahun_lulus'] = $this->input->post('tahun_lulus')[0];
                $arrpendidikan['nama_sekolah'] = $this->input->post('nama_sekolah')[0];
                $this->Data_model->simpanData($arrpendidikan, 't_pendidikan');
            }
            $this->Data_model->simpanData($arrdata, 't_' . $this->uri->segment(3));
        } else {

            $kondisi = 'nip';

            if (is_array($this->input->post('id_pendidikan'))) {
                $q = "DELETE FROM t_pendidikan WHERE nip IN (" . $this->input->post('nip') . ")";
                $this->Data_model->jalankanQuery($q);
                for ($i = 0; $i < count($this->input->post('id_pendidikan')); $i++) {
                    $arrpendidikan['nip'] = $this->input->post('nip');
                    $arrpendidikan['id_pendidikan'] = $this->input->post('id_pendidikan')[$i];
                    $arrpendidikan['tahun_lulus'] = $this->input->post('tahun_lulus')[$i];
                    $arrpendidikan['nama_sekolah'] = $this->input->post('nama_sekolah')[$i];
                    $this->Data_model->simpanData($arrpendidikan, 't_pendidikan');
                    // print_r($arrpendidikan);
                }
                // echo $this->db->last_query();
                // die;
            }
            $this->Data_model->updateDataWhere($arrdata, 't_' . $this->uri->segment(3), array($kondisi => $cid));
        }
    }

    function hapus() {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('nip' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('nip' => $param);
                }
                $this->Data_model->hapusDataWhere('t_' . $this->input->post('cod'), $kondisi);
                $this->Data_model->hapusDataWhere('t_aplikasi', $kondisi);
                $this->Data_model->hapusDataWhere('t_pendidikan', $kondisi);
                $this->Data_model->hapusDataWhere('t_kerja', $kondisi);
                echo json_encode("ok");
            }
        }
    }

    function view_detail($nip) {
        $nip = $this->uri->segment(3);
        $kondisi = "nip";
        $data['rowdata'] = $this->Data_model->satuData('vpegawai', array($kondisi => $nip));
        $data['rowaplikasi'] = $this->Data_model->jalankanQuery("SELECT nama_aplikasi, username FROM t_aplikasi JOIN m_aplikasi ON m_aplikasi.kode = t_aplikasi.id_aplikasi WHERE nip = '" . $nip . "'", 3);
        $data['rowpendidikan'] = $this->Data_model->jalankanQuery("SELECT nama_pendidikan, tahun_lulus, nama_sekolah FROM t_pendidikan JOIN m_pendidikan ON m_pendidikan.kode = t_pendidikan.id_pendidikan WHERE nip = '" . $nip . "'", 3);
        $data['rowkerja'] = $this->Data_model->jalankanQuery("SELECT * FROM t_kerja WHERE nip = '" . $nip . "'", 3);
        $this->load->view('view_pegawai', $data);
    }

    function getList() {
        $tabel = $this->input->post('tabel');
        $param = $this->input->post('param');
        $field = $this->input->post('fld');
        if (strpos($this->input->post('kolom'), ',')) {
            $arrnmfld = explode(',', $this->input->post('kolom'));
            $nmfld1 = $arrnmfld[0];
            $nmfld2 = $arrnmfld[1];
            $grpby = array($nmfld1, $nmfld2);
        } else {
            $nmfld1 = $this->input->post('kolom');
            $nmfld2 = $this->input->post('kolom');
            $grpby = array($nmfld1);
        }
        if (strpos($this->input->post('fld'), ',')) {
            $f = explode(",", $field);
            $p = explode("-", $param);
            $kondisi = array($f[0] => $p[0], $f[1] => $p[1]);
        } else {
            $kondisi = array($field => $param);
        }
        $order = "1";
        $query = $this->db->select("$nmfld1,$nmfld2")->order_by($order, 'ASC')->group_by($grpby)->get_where($tabel, $kondisi);
        // echo $this->db->last_query(); die;
        $result = $query->result();
        $data = array();
        if (!empty($result)) {
            foreach ($result as $row) {
                $data[] = '{"id" : "' . $row->$nmfld1 . '", "value" : "' . trim($row->$nmfld2) . '", "nomor": "' . $row->$nmfld1 . '", "sub":0, "isigrup":0}';
            }
        }
        if (IS_AJAX) {
            echo '[' . implode(',', $data) . ']';
            // die;
        }
    }

}
