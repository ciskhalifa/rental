<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/js/functions.js" type="text/javascript"></script>        
<script src="<?= base_url() ?>assets/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<div id="myConfirm" class="modal fade">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Ã—</span></button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-outline">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var myApp = myApp || {};
    $(function () {
        $('#containerform').hide();
//        $('#tabelpegawai').hide();
        $('#containerdetail').hide();
        $('#openform').on('click', function () {
            $('#tabelpegawai').hide();
            $('#formpegawai').load('peminjaman/form/' + $("#tabel").val());
            $('#containerform').fadeIn('fast');
        });
        myTableCIS("#pegawaiTable",
                {
                    ajaxSource: "peminjaman/listData/" + $('#tabel').val(),
                    columnDefs: [
                        {targets: [0], sortable: false, sWidth: "35px"},
                        {targets: [0, 8], class: "text-center"},
                        {targets: [8], render: function (data, type, row) {
                                var kode = piceunSpasi(data);
                                var display = '';
                                var display = display + ' <button class="btn btn-info waves-effect"  data-kode="' + kode + '" data-bind="ubah" data-tabel="pegawai"><i class="icon-edit2"></i></button>';
                                display = display + ' <button class="btn btn-danger waves-effect" data-value="' + kode + '" data-bind="hapus" data-get="pegawai/hapus" data-mtabel="pegawai/hapus" data-default="' + row[1] + '"><i class="icon-trash-o"></i></button>';
                                display = display + ' <button class="btn btn-warning  waves-effect" data-kode="' + kode + '" data-bind="view" data-get="view_detail"><i class="icon-eye6"></i></button>';

                                return display;
                            }, sortable: false, sClass: "center p-r-0", sWidth: "125px"}
                    ]
                }
        );
        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(), dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    myApp.oTable.fnDraw(false);
                    $("#myConfirm").modal("hide")
                }})
        });
    });
</script>


