<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = $arey['kode'];
}else {
    $cid = '';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form Peminjaman</h3>
                </div>
                <div class="box-body">
                    <input type="hidden" id="tabel" value="peminjaman">
                    <form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
                        <div class="form-body">
                            <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">                            
                            <div class="form-group">
                                <label class="col-md-2 label-control">Konsumen</label>
                                <div class="col-md-4">
                                    <select name="kode_konsumen" class="form-control select2">
                                        <option value="">- Pilihan -</option>
                                        <?php
                                        $n = (isset($arey)) ? $arey['kode_konsumen'] : '';
                                        $q = $this->Data_model->selectData("m_konsumen", "kode");
                                        foreach ($q as $row):
                                            $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                            ?>
                                            <option value="<?= $row->kode; ?>" <?= $kapilih; ?>><?= $row->nama_lengkap; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <table class="table" id="barang" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:20px">No</th>
                                                <th>Barang</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (empty($rowbarang)) { ?>
                                                <tr id="row1">
                                                    <td style="text-align:center">1</td>
                                                    <td>
                                                        <select class="pilihbarang form-control" id="selectbarang1" name="kode_barang[]"></select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="tahun_lulus1">
                                                    </td>
                                                </tr>
                                            <?php } else if (isset($rowbarang)) { ?>

                                                <?php
                                                $no = 0;
                                                foreach ($rowbarang as $row):
                                                    ?>
                                                    <tr id="row<?php echo($no + 1); ?>" >
                                                        <td style="text-align:center"><?php echo($no + 1); ?></td>
                                                        <td>
                                                            <select name="kode_barang[]" id="selectbarang<?php echo($no + 1); ?>" data-default="<?php echo $row->kode_barang; ?>" class="select2 pilihbarang form-control" data-live-search="true">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="tahun_lulus<?php echo ($no + 1); ?>" value="<?php echo $row->jumlah; ?>">
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $no++;
                                                endforeach;
                                                ?>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3">
                                                    <button type="button" class="btn btn-danger removebarang">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-primary addbarang">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 label-control">Tanggal Sewa</label>
                                <div class="col-md-2">
                                    <input type="text" data-provide="datepicker" class="form-control input-sm date" name="tgl_sewa" id="tgl_sewa" value="<?= (isset($arey)) ? $arey['tgl_sewa'] : ''; ?>" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 label-control">Tanggal Kembali</label>
                                <div class="col-md-2">
                                    <input type="text" data-provide="datepicker" class="form-control input-sm date" name="tgl_kembali" id="tgl_kembali" value="<?= (isset($arey)) ? $arey['tgl_kembali'] : ''; ?>" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a href="javascript:" class="btn btn-primary" id="saveform"><i class="icon-check2"></i> Simpan</a>
                                <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function () {
        $("#tmblBatal").on("click", function () {
            window.location.replace("<?= base_url('peminjaman'); ?>");
        });
        $("#saveform").on("click", function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            }
            var link = 'simpanData'
            var sData = new FormData($('#xfrm')[0]);
            $.ajax({
                url: link,
                type: "POST",
                data: sData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "html",

                beforeSend: function () {

                },
                success: function (html) {
                    window.location.replace("<?= base_url('peminjaman'); ?>");
                }
            });
            return false;
        });
        var a = $("#barang tbody tr").length;
        $(".addbarang").on("click", function () {
            a++;
            var x = '<td style="text-align: center;">' + (a) + "</td>";
            x += '<td><select class="select2 pilihbarang form-control" id="selectbarang' + (a) + '" name="kode_barang[]"></select></td>';
            x += '<td><input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="jumlah' + (a) + '"></td>';
            $("#barang tbody").append('<tr id="row' + (a) + '">' + x + "</tr>");
            selectBarang(a);
        });
        $(".removebarang").on("click", function () {
            if ($("#barang tbody tr").length > 1) {
                $("#barang tbody tr:last-child").remove();
                a--;
            } else {
                alert("Baris pertama isian tidak dapat dihapus")
            }
        });

        bukaListBarang();
        function bukaListBarang() {
            if ($("#cid").val() == "") {
                selectBarang(1);
            } else {
                $.each($(".pilihbarang"), function (j, g) {
                    var c = "selectbarang" + (j + 1);
                    var b = $("#selectbarang" + (j + 1)).attr("data-default");
                    getListCIS("m_barang", c, "1", "1", $("#selectbarang" + (j + 1)).attr("data-default"), "kode,nama_barang", "Pilihan Barang", "getListBarang");
                    $(".select").select2();

                });

            }
        }

        function selectBarang(urutan) {
            $.ajax({
                url: "peminjaman/select_barang",
                type: "POST",
                success: function (html) {
                    json = eval(html);
                    $("#selectbarang" + urutan + " ").append('<option value="">Pilihan</option>');
                    $(json).each(function () {
                        $("#selectbarang" + urutan + " ").append('<option value="' + this.kode + '">' + this.disp + " | Tersedia : " + this.stok + "</option>")
                    });
                    $(".select").select2();
                }
            })
        }
    }
    );
</script>