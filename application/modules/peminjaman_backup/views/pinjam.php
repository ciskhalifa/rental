<div id="tabelpegawai" class="slideInDown animated" data-appear="appear" data-animation="slideInDown">
    <input type="hidden" id="tabel" value="<?php echo $tabel; ?>">
    <input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
    <div class="content-detached">
        <div class="content-body">
            <section class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"> List <?= $this->uri->segment('1'); ?></h4>
                            <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                            <div class="heading-elements">
                                <a href="javascript:" class="btn btn-primary btn-md" id="openform" data-tab="" data-original-title="Tambah Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                    <i class="icon-plus4 white"></i>
                                </a>
                                <a href="javascript:" class="btn btn-warning btn-md" id="import" data-tab="" data-original-title="Import Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                    <i class="icon-edit2 white"></i>
                                </a>
                                <a href="javascript:" class="btn btn-info btn-md" id="report" data-tab="" data-original-title="Export Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                    <i class="icon-android-download white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <!-- Task List table -->
                                <table id="pegawaiTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div id="containerform" class="content-detached content-right">
    <div class="content-body">
        <section class="row">
            <div class="col-xs-10">
                <div class="card">
                    <div class="card-block">
                        <div class="card-body card-padding">
                            <form role="form" id="xfrm" class="form form-horizontal" enctype="multipart/form-data">
                                <div class="form-body">
                                    <h4 class="form-section"><i class="icon-head"></i> Form Pegawai</h4>
                                    <div id="formpegawai"></div>
                                    <div class="form-actions">
                                        <button type="button" id="saveform" class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
                                        <button type="button" id="cancelform" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div id="containerdetail" class="content-detached">
    <div class="content-body">
        <div class="col-md-12">
            <div id="contentdetail">

            </div>
        </div>
    </div>
</div>
