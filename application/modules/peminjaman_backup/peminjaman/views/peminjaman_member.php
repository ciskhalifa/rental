<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Peminjaman</h3>
                    <div class="box-tools pull-right">
                        <?php if ($_SESSION['role'] == 4): ?>
                            <a type="button" class="btn btn-box-tool" id="add" href="<?= base_url('peminjaman/formmember') ?>"><i class="fa fa-plus"></i></a>
                        <?php elseif ($_SESSION['role'] == 1): ?>
                            <a type="button" class="btn btn-box-tool" id="add" href="<?= base_url('peminjaman/form') ?>"><i class="fa fa-plus"></i></a>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="box-body">
                    <table id="data-peminjaman" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </thead>    
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($rowdata as $row):
                                ?>
                                <tr>
                                    <td><?= $row->kode; ?></td>
                                    <td><?= "TRX00" . $no++; ?></td>
                                    <td><?= $row->nama_lengkap; ?></td>
                                    <td><?= $row->tgl_sewa; ?></td>
                                    <td><?= $row->tgl_kembali; ?></td>
                                    <td><?= $row->stat; ?></td>
                                    <td>
                                        <a id="btnDetail" class="btn btn-info btn-md btnDetail" href="<?= base_url('peminjaman/detail/') . $row->kode ?>"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="myConfirm" class="modal fade">
    <div class="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">

                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>