<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
date_default_timezone_set("Asia/Bangkok");
$tgl_sewa = date_create($rowdata[0]->tgl_sewa);
$tgl_kembali = date_create($rowdata[0]->tgl_kembali);
$diff = date_diff($tgl_sewa, $tgl_kembali);
$dat = $diff->format("%a");

$harini = date_create(date("Y/m/d"));
$denda = date_diff($harini, $tgl_kembali);
$den = $denda->format("%a");

if (date("Y-m-d") > $rowdata[0]->tgl_kembali) {
    $ax = date_diff($harini, $tgl_kembali);
    $dens = $ax->format("%a");
} else {
    $dens = 0;
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Detail Peminjam</h3>
                    <div class="box-tools pull-right">
                        <button onclick="myFunction()" class="cart-link" title="View Cart">
                            <i class="glyphicon glyphicon-print"></i>
                        </button>
                    </div>
                </div>
                
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-3 label-control">Nama Lengkap :</label>
                        <div class="col-md-8">
                            <label class="col-md-6 label-control"><?= $rowdata[0]->nama_lengkap; ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 label-control">Alamat Lengkap :</label>
                        <div class="col-md-8">
                            <label class="col-md-6 label-control"><?= $rowdata[0]->alamat; ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 label-control">No Telp :</label>
                        <div class="col-md-8">
                            <label class="col-md-6 label-control"><?= $rowdata[0]->no_telp; ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 label-control">Jaminan :</label>
                        <div class="col-md-8">
                            <label class="col-md-6 label-control"><?= $rowdata[0]->no_identitas; ?></label>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Barang</h3>
                </div>
                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Harga / Hari</th>
                        <th>Subtotal</th>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($rowdata as $data):
                                $subtot = ($data->jumlah * $data->harga_sewa) * $dat;
                                
                                if ($rowdata[0]->statkonsumen == "member") {
                                    $a[] = $subtot;
                                    $b[] = $den * $subtot;
                                    $c = $a + $b;
                                    $d = 0.05 * $subtot;
                                    $e = $subtot - $d + ($den * $subtot);
                                    $f = array_sum($a) - $d;
                                } else {
                                    $a[] = $subtot;
                                    $b[] = $den * $subtot;
                                    $c = $a + $b;
                                    $d = 0;
                                    $e = $subtot - $d + ($den * $subtot);
                                    $f = array_sum($a) - $d;
                                }
                                ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $data->nama_barang; ?></td>
                                    <td><?= $data->jumlah; ?></td>
                                    <td><?= "Rp. " . number_format($data->harga_sewa, 2, ',', '.'); ?></td>
                                    <td><?= "Rp. " . number_format($subtot, 2, ',', '.'); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <?php $c = array_sum($a) + array_sum($b); ?>
                            <tr>
                                <td colspan="5"><label class="col-md-4 label-control pull-right"><?= "Subtotal : Rp " . number_format(array_sum($a), 2, ',', '.'); ?></label></td>
                            </tr>
                            <tr>
                                <td colspan="5"><label class="col-md-4 label-control pull-right"><?= "Diskon : Rp " . number_format($d, 2, ',', '.'); ?></label></td>
                            </tr>
                            <tr>
                                <td colspan="5"><label class="col-md-4 label-control pull-right"><?= "Total : Rp " . number_format($f, 2, ',', '.'); ?></label></td>
                        <input type="hidden" value="<?= $e ?>" class="totalbayar">
                        <input type="hidden" value="<?= $rowdata[0]->kode; ?>" class="id_trans">
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>