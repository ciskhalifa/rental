<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = $arey['kode'];
}else {
    $cid = '';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form Pembelian</h3>
                </div>
                <div class="box-body">
                    <input type="hidden" id="tabel" value="peminjaman">
                    <form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
                        <div class="form-body">
                            <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table class="table" id="barang" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:20px">No</th>
                                                <th>Kategori</th>
                                                <th>Merk</th>
                                                <th>Barang</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                                <th>Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (empty($rowbarang)) { ?>
                                                <tr id="row1">
                                                    <td style="text-align:center">1</td>
                                                    <td>
                                                        <select class="pilihkategori form-control" id="selectkategori1" name="kode_kategori[]">
                                                            <option value="">- Pilihan -</option>
                                                            <?php
                                                            $n = (isset($arey)) ? $arey['kode_kategori'] : '';
                                                            $q = $this->Data_model->get_kategori();
                                                            foreach ($q as $row):
                                                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                                                ?>
                                                                <option value="<?= $row->kode; ?>" <?= $kapilih; ?>><?= $row->nama_kategori; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="pilihmerk form-control" id="selectmerk1" name="kode_merk[]"><option value="">- Pilihan -</option></select>
                                                    </td>
                                                    <td>
                                                        <select class="pilihbarang form-control" id="selectbarang1" name="kode_barang[]"><option value="">- Pilihan -</option></select>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="jumlah[]" class="form-control" placeholder="Jumlah Barang" id="jumlah1">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="harga[]" class="form-control" placeholder="Harga Barang" id="harga1">
                                                    </td>
                                                    <td>
                                                        <input type="date" name="tgl[]" class="form-control" placeholder="Tanggal" id="tgl1">
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3">
                                                    <button type="button" class="btn btn-danger removebarang">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-primary addbarang">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a href="javascript:" class="btn btn-primary" id="saveform"><i class="icon-check2"></i> Simpan</a>
                                <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


