<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<script src="<?=base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Select2 -->
<link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/select2/dist/css/select2.min.css');?>">
<!-- Select2 -->
<script src="<?=base_url('assets/admin/bower_components/select2/dist/js/select2.full.min.js');?>"></script>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url('assets/admin/plugins/iCheck/all.css');?>">
<!-- iCheck 1.0.1 -->
<script src="<?=base_url('assets/admin/plugins/iCheck/icheck.min.js');?>"></script>
<script src="<?=base_url('assets/jquery.chained.min.js')?>"></script>
<script>
    var myApp = myApp || {};
    $(function () {
        var date = new Date();
        date.setDate(date.getDate());
        $('#tgl_beli').datepicker({
            format: 'yyyy-mm-dd',
            startDate: date
        });

        $('#data-pembelian').DataTable();
        $('#data-pembelian').on('click', '.btnDel', function () {
            $('#getto').val($(this).attr("data-get"));
            $('#aepYes').text('Hapus');
            $("#cid").val($(this).attr("data-id"));
            $("#cod").val($(this).attr("data-mtabel"));
            $('.lblModal').text('hapus ' + $(this).attr("data-default"));
            $('#myConfirm').modal();
        });
        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=pinjam", dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    notify("Delete berhasil", "danger")
                    $("#myConfirm").modal("hide")
                    location.reload(true);
                }})
        });
        $("#tmblBatal").on("click", function () {
            window.location.replace("<?=base_url('pembelian');?>");
        });
        $("#saveform").on("click", function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            }
            var link = 'simpanData'
            var sData = $("#xfrm").serialize();
            $.ajax({
                url: link,
                type: "POST",
                data: sData,
                dataType: "html",
                beforeSend: function () {

                },
                success: function (html) {

                    window.location.replace('pembelian');
                }
            });
            return false;
        });
         var a = $("#barang tbody tr").length;
         $(".addbarang").on("click", function () {
            a++;
            var x = '<td style="text-align: center;">' + (a) + "</td>";
            x += '<td><select class="select2 pilihkategori form-control" id="selectkategori' + (a) + '" name="kode_kategori[]"></select></td>';
            x += '<td><select class="select2 pilihmerk form-control" id="selectmerk' + (a) + '" name="kode_merk[]"></select></select></td>';
            x += '<td><select class="select2 pilihbarang form-control" id="selectbarang' + (a) + '" name="kode_barang[]"></select></td>';
            x += '<td><input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="jumlah' + (a) + '"></td>';
            x += '<td> <input type="text" name="harga[]" class="form-control" placeholder="Harga Barang" id="harga' + (a) + '"></td>';
            x += '<td><input type="date" class="form-control" placeholder="Jumlah Barang" name="tgl[]" id="harga' + (a) + '"></td>';
            $("#barang tbody").append('<tr id="row' + (a) + '">' + x + "</tr>");
            //$("#selectmerk" + a).chained("#selectkategori" + a);
            selectKategori(a);
            kategori(a);
            merk(a);
        });
        $(".removebarang").on("click", function () {
            if ($("#barang tbody tr").length > 1) {
                $("#barang tbody tr:last-child").remove();
                a--;
            } else {
                alert("Baris Habis");
            }
        });
        kategori(1);
        merk(1);
        function kategori(urutan) {
            $('#selectkategori' + urutan).on('change', function () {
                var kategori_id = $(this).val();
                if (kategori_id) {
                    $.ajax({
                        type: 'POST',
                        url: '../peminjaman/getMerk',
                        data: 'kategori_id=' + kategori_id,
                        success: function (html) {
                            $("#selectmerk" + urutan).html(html);
                            $("#selectbarang" + urutan).html('<option value="">- Pilihan -</option>');
                        }
                    });
                } else {
                    $("#selectmerk" + urutan).html('<option value="">- Pilihan -</option>');
                    $("#selectbarang" + urutan).html('<option value="">- Pilihan -</option>');
                }
            });
        }
        function merk(urutan) {
            $('#selectmerk' + urutan).on('change', function () {
                var merk_id = $(this).val();
                if (merk_id) {
                    $.ajax({
                        type: 'POST',
                        url: '../peminjaman/getBarang',
                        data: 'merk_id=' + merk_id,
                        success: function (html) {
                            $("#selectbarang" + urutan).html(html);
                        }
                    });
                } else {
                    $("#selectbarang" + urutan).html('<option value="">- Pilihan -</option>');
                }
            });
        }
        function selectKategori(urutan) {
            $.ajax({
                url: "../peminjaman/select_kategori",
                type: "POST",
                success: function (html) {
                    json = eval(html);
                    $("#selectkategori" + urutan + " ").append('<option value="">- Pilihan -</option>');
                    $(json).each(function () {
                        $("#selectkategori" + urutan + " ").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                    });
                    $(".select").select2();
                }
            })
        }
    });
</script>