<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        $data['content'] = 'pembelian';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("Kode Pembelian", "Kategori", "Merk", "Barang", "Jumlah", "Harga", "Tanggal Beli", "Total", "Opsi");
        $sTable = 'vbeli';
        $sWhere = "";
        $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere ";
        $data['rowdata'] = $this->Data_model->jalankanQuery($tQuery, 3);
        $this->load->view('default', $data);
    }

    public function form()
    {
        $nilai = $this->uri->segment(3);
        $data['content'] = 'pembelian/form_pembelian';
        $data['css'] = 'css';
        $data['js'] = 'js';
        if ($nilai != "") {
            $data['rowdata'] = $this->Data_model->satuData('t_beli', array('kode' => $nilai));
        }
        $this->load->view('default', $data);
    }

    public function simpanData()
    {
        $arrdata = array();
        $cid = '';
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {

            } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                switch (trim($key)):
            case 'cid':
                $cid = $value;
                break;
            default:
                if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("-", $value);
                            $newtgl = $tgl[1] . "-" . $tgl[0] . "-" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = $value; // date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                } else {
                    $arrdata[$key] = $value;
                }
                break;
                endswitch;
            }
        }
        if (is_array($this->input->post('kode_kategori'))) {
            for ($i = 0; $i < count($this->input->post('kode_kategori')); $i++) {
                $arrdata['kode_merk'] = $this->input->post('kode_kategori')[$i];
                $arrdata['kode_kategori'] = $this->input->post('kode_kategori')[$i];
                $arrdata['kode_barang'] = $this->input->post('kode_barang')[$i];
                $arrdata['tgl'] = $this->input->post('tgl')[$i];
                $arrdata['jumlah'] = $this->input->post('jumlah')[$i];
                $arrdata['harga'] = $this->input->post('harga')[$i];
                $arrdata['status'] = '0';
                $this->Data_model->simpanData($arrdata, 't_beli');
            }
        } else {
            $arrdata['kode_merk'] = $this->input->post('kode_kategori')[0];
            $arrdata['kode_kategori'] = $this->input->post('kode_kategori')[0];
            $arrdata['kode_barang'] = $this->input->post('kode_barang')[0];
            $arrdata['tgl'] = $this->input->post('tgl')[0];
            $arrdata['jumlah'] = $this->input->post('jumlah')[0];
            $arrdata['harga'] = $this->input->post('harga')[0];
            $arrdata['status'] = '0';
            $this->Data_model->simpanData($arrdata, 't_beli');
        }
        echo $this->db->last_query();
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('t_beli', $kondisi);
                echo json_encode("ok");
            }
        }
    }

}
