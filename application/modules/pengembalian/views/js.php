<script src="<?=base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script>
    $(function () {
        $('#data-pengembalian').DataTable();
        $('.btnStat').bind('click', function () {
            var hari = [];
            var link2 = 'pengembalian/getBarang';
            $.ajax({
                url: link2, type: "POST", data: "cid=" + $(this).attr("data-id"), dataType: "json", beforeSend: function() {
                    if (link2 != "#") {

                    }
                },
                success: function(html) {
                    for (var x = 0; x < html.length; x++) {
                        hari.push(html[x].hari);
                    }
                    //var obj = JSON.parse(html);
                    
                    for (i = 0; i < html.length; i++) {
                        var x = '<td>' +html[i].kode+ '</td>';
                        x += '<td>' +html[i].nama+ '</td>';
                        x += '<td>' + html[i].jumlah + '</td>';
                        x += '<td>'+hari[i]+'</td>';
                        $("#barang tbody").append('<tr id="row' + (i) + '">' + x + "</tr>");
                    }
                    $("#barang tr").click(function(){
                        $(this).toggleClass('selected');
                        var value=$(this).find('td:first').html();
                        var selected = [];
                        $("#barang tr.selected").each(function(){
                            selected.push($('td:first', this).html());
                        });
                        $('.kode_barang').val(selected);
                    });
                }
            });
            $('#getto').val($(this).attr("data-get"));
            $('#aepYes').text('Hapus');
            $("#cid").val($(this).attr("data-id"));
            $('.id_trans').val($(this).attr("data-id"));
            $("#cod").val($(this).attr("data-mtabel"));
            $('.lblModal').text('Update Status Pada Data ' + $(this).attr("data-default"));
            $('#myConfirm').modal();
        });
        $('#btnCancel').click(function () {
            $("#barang tbody").empty();
            $('#myConfirm').modal('hide');
        });
        $('#btnCancel2').click(function () {
            $("#barangdenda tbody").empty();
            $('#myConfirm').modal('hide');
        });
        $('.btnStat2').bind('click', function () {

            $("#cid").val($(this).attr("data-id"));
            $('.id_trans').val($(this).attr("data-id"));
            var hari = [];
            var link = 'pengembalian/cekdata';
            $.ajax({
                url: link, type: "POST", data: "cid=" + $("#cid").val(), dataType: "json", beforeSend: function() {
                    if (link != "#") {

                    }
                },
                success: function(html) {
                    var itung = 0;
                    for (var x = 0; x < html.length; x++) {
                        itung += html[x].denda;
                        hari.push(html[x].hari);
                    }
                    $('.denda').val(itung);
                    $('.bayar_denda').val(html[0].bayar_denda);
                    $('.kembalian_denda').val(html[0].kembalian_denda);
                    $('.ket_denda').val(html[0].ket_denda);

                }
            });
            var link2 = 'pengembalian/getBarang';
            $.ajax({
                url: link2, type: "POST", data: "cid=" + $("#cid").val(), dataType: "json", beforeSend: function() {
                    if (link2 != "#") {

                    }
                },
                success: function(html) {
                    //var obj = JSON.parse(html);

                    for (i = 0; i < html.length; i++) {
                        var x = '<td>' +html[i].kode+ '</td>';
                        x += '<td>' +html[i].nama+ '</td>';
                        x += '<td>' + html[i].jumlah + '</td>';
                        x += '<td>'+hari[i]+'</td>';
                        $("#barangdenda tbody").append('<tr id="row' + (i) + '">' + x + "</tr>");
                    }

                    $("#barangdenda tr").click(function(){
                        $(this).toggleClass('selected');
                        var value=$(this).find('td:first').html();
                        var selected = [];
                        $("#barangdenda tr.selected").each(function(){
                            selected.push($('td:first', this).html());
                        });
                        $('.kode_barang').val(selected);
                    });

                }
            });

            $('#myConfirm2').modal();
            $('#getto').val($(this).attr("data-get"));
            $('#aepYes').text('Hapus');
            $("#cid").val($(this).attr("data-id"));
            $("#cod").val($(this).attr("data-mtabel"));
            
            // $('.lblModal').text('Update Status Pada sss ' + $(this).attr("data-default"));
            // $('#myConfirm2').modal();
        });

        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=pinjam&kode_barang="+$('.kode_barang').val(), dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    notify("Berhasil diupdate", "success")
                    $("#myConfirm").modal("hide")
                    location.reload(true);
                }})
        });
        $('#btnYesDenda').bind('click', function(){
            var link = 'pengembalian/getBayar'
            $.ajax({
                    type: 'POST',
                    url: link,
                    data: {'kode_barang': $('.kode_barang').val(), 'ket_denda': $('.ket_denda').val(), 'id_trans': $('.id_trans').val(), 'kembalian': $('.kembalian_denda').val(), 'bayar': $('.bayar_denda').val(), 'denda': $('.denda').val()},
                    success: function (html) {
                        location.reload(true);
                        document.getElementById('bayar').disabled = true;
                    }
                })
        });
        $('.bayar_denda').on('change', function () {
            //console.log($('.denda').val());
            var totalbayar = $('.denda').val();
            var bayar_denda = $(this).val();
            var kode_transaksi = $('.id_trans').val();
            console.log($(this).val());
            var link = '../pengembalian/getBayar'
            if (bayar_denda >= totalbayar) {
                var kembalian = $(this).val() - totalbayar;
                $('.kembalian_denda').val(kembalian);
            } else {
                alert("Dana Kurang");
                document.getElementById('bayar_denda').disabled = false;
            }

        });


    });


</script>