<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Pengembalian</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <table id="data-pengembalian" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                $no = 1;
                                foreach ($rowdata as $row):
                                    $kode[] = $row->kode;
                                    ?>
		                                <tr>
		                                    <td><?=$row->kode;?></td>
		                                    <td><?="TRX00" . $no++;?></td>
		                                    <td><?=$row->nama_lengkap;?></td>
		                                    <td><?=$row->tgl_sewa;?></td>
		                                    <td><?=$row->tgl_kembali;?></td>
		                                    <td> 
                                            <?php if ($row->status == "Tidak Denda") : ?>
                                            <span class="btn btn-success"><?= $row->status ?>
                                            <?php else: ?>
                                            <span class="btn btn-danger"><?= $row->status ?>
                                            <?php endif; ?>
                                            </td>
                                            </td>
		                                    <td>
		                                        <?php if ($row->status == "Tidak Denda"): ?>
		                                        <button id="btnStat" class="btn btn-success btn-md btnStat"  data-mtabel="t_datin" data-get="pengembalian/gantiStatus" data-id="<?=$row->kode;?>" data-default="<?=$row->kode;?>"><i class="fa fa-check-circle"></i></button>
		                                        <?php else: ?>
                                        <button id="btnStat2" class="btn btn-success btn-md btnStat2"  data-mtabel="t_datin" data-get="pengembalian/gantiStatus" data-id="<?=$row->kode;?>" data-default="<?=$row->kode;?>"><i class="fa fa-check-circle"></i></button>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="myConfirm" class="modal fade">
        <div class="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                    <table class="table table-hover" id="barang">
                            <thead>
                                <th>Kode</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Pinjam</th>
                                <th>Jumlah Hari</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br><hr>
                        <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" class="kode_barang">
                        <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                        <button type="button" id="btnCancel" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                        <button type="button" id="btnYes" class="btn btn-success">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myConfirm2" class="modal fade">
        <div class="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover" id="barangdenda">
                            <thead>
                                <th>Kode</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Pinjam</th>
                                <th>Jumlah Hari</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br><hr>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-4 label-control">Denda</label>
                                <div class="col-md-6">
                                    <input type="text" disabled class="form-control input-sm denda" name="denda" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 label-control">Bayar Denda</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control input-sm bayar_denda" id="bayar_denda" name="bayar_denda" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 label-control">Keterangan Denda</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control input-sm ket_denda" name="ket_denda" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 label-control">Kembalian Denda</label>
                                <div class="col-md-6">
                                    <input type="text" disabled class="form-control input-sm kembalian_denda" name="kembalian_denda" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="" class="id_trans">
                        <input type="hidden" class="kode_barang">
                        <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                        <button type="button" id="btnCancel2" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                        <button type="button" id="btnYesDenda" class="btn btn-success">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
