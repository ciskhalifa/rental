<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Pengembalian extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        $data['content'] = 'pengembalian';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("No", "Kode Pinjam", "Nama Lengkap", "Tanggal Sewa", "Tanggal Kembali", "Status", "Opsi");
        $sTable = 'vpinjam';
        $sWhere = " stat = 'Dipinjamkan'";
        $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE $sWhere ";
        $data['rowdata'] = $this->Data_model->jalankanQuery($tQuery, 3);
        $this->load->view('default', $data);
    }
    public function getBarang()
    {
        $param = $this->input->post('cid');
        $q = $this->Data_model->jalankanQuery("SELECT * FROM detail_pinjam WHERE status='Dipinjamkan' OR status IS NULL AND kode_pinjam=" . $param, 3);
        foreach ($q as $row) {
            $q = $this->Data_model->jalankanQuery("SELECT * FROM m_barang WHERE kode=" . $row->kode_barang, 3);
            foreach ($q as $data) {
                $a[] = array('kode' => $data->kode, 'nama' => $data->nama_barang, 'jumlah' => $row->jumlah);

            }
        }
        echo json_encode($a);

    }
    public function gantiStatus()
    {
        if (IS_AJAX) {
            $k = explode(',', $_POST['kode_barang']);
            $param = $this->input->post('cid');
            
            $kosong = array();
            $arr = array();
            if ($k[0] !== ''){
                $proses = $this->statusDetail($k, $param);
            }else{
                $proses = $this->statusDetail($kosong, $param);
            }
            $q = $this->Data_model->jalankanQuery("SELECT * FROM detail_pinjam WHERE kode_pinjam=" . $param, 3);
            $a = $this->Data_model->jalankanQuery("SELECT * FROM detail_pinjam WHERE status IS NULL AND kode_pinjam=".$param, 3);
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $arrdata = array('status' => 'Dikembalikan', 'denda' => $this->input->post('denda'));

                foreach ($q as $row) {

                    $q = $this->Data_model->jalankanQuery("SELECT * FROM m_barang WHERE kode=" . $row->kode_barang, 3);
                    foreach ($q as $data) {
                        $kode[] = $data->kode;
                        $stok[] = $data->stok + $row->jumlah;
                    }
                }
                for ($i = 0; $i < count($stok); $i++) {
                    $this->Data_model->updateDataWhere(array('stok' => $stok[$i]), 'm_barang', array('kode' => $kode[$i]));
                }

                if (count($a) == 0){
                    $arr['status'] = 'Dikembalikan';
                    $this->Data_model->updateDataWhere($arr, 't_pinjam', array('kode' => $param));
                }else{
                    $arr['status'] = 'Dipinjamkan';
                    $this->Data_model->updateDataWhere($arr, 't_pinjam', array('kode' => $param));
                }

                echo json_encode("ok");
            }
        }
    }

    public function cekdata()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            $q = $this->Data_model->jalankanQuery("SELECT * FROM vdetail WHERE kode='" . $param . "'", 3);
            $x = $this->Data_model->jalankanQuery("SELECT * FROM t_pinjam WHERE kode='" . $param. "'", 3);
            date_default_timezone_set("Asia/Bangkok");
            $tgl_sewa = date_create($q[0]->tgl_sewa);
            $tgl_kembali = date_create($q[0]->tgl_kembali);
            $diff = date_diff($tgl_sewa, $tgl_kembali);
            $dat = $diff->format("%a");

            $harini = date_create(date("Y/m/d"));
            $denda = date_diff($harini, $tgl_kembali);
            $den = $denda->format("%a");

            if (date("Y-m-d") > $q[0]->tgl_kembali) {
                $ax = date_diff($harini, $tgl_kembali);
                $dens = $ax->format("%a");
            } else {
                $dens = 0;
            }
            foreach ($q as $rowdata) {
                $subtot = ($rowdata->jumlah * $rowdata->harga_sewa) * $dat;
                $a[] = $subtot;
                $b[] = array('denda' => $dens * $subtot, 'hari' => $dat, 'bayar_denda' => $x[0]->bayar_denda, 'kembalian_denda' => $x[0]->kembalian_denda, 'ket_denda' => $x[0]->ket_denda);
                // $f = $dens * array_sum($subtot);
            }

            echo json_encode($b);
        }
    }
    public function statusDetail($data, $id){
        if (empty($data)){
            return 'habis';
        }else{
            for($i = 0; $i < count($data); $i++){
                $this->Data_model->updateDataWhere(array('status' => "Dikembalikan"), 'detail_pinjam', array('kode_pinjam' => $id, 'kode_barang' => $data[$i]));
            }
            return 'belum';
        }
    }
    public function getBayar()
    {
       
        $id = $_POST['id_trans'];
        $k = explode(',', $_POST['kode_barang']);
        $kosong = array();
       
        if ($k[0] !== ''){
            $proses = $this->statusDetail($k, $id);
        }else{
            $proses = $this->statusDetail($kosong, $id);
        }
        $kondisi = 'kode';
        $arrdata = array('ket_denda' => $_POST['ket_denda'], 'bayar_denda' => $_POST['bayar'], 'kembalian_denda' => $_POST['kembalian'], 'denda' => $_POST['denda']);
        $q = $this->Data_model->jalankanQuery("SELECT * FROM detail_pinjam WHERE status IS NULL AND kode_pinjam=" . $id, 3);
        
        if (count($q) == 0){
            $arrdata['status'] = 'Dikembalikan';
            $this->Data_model->updateDataWhere($arrdata, 't_pinjam', array($kondisi => $id));
        }else{
            $this->Data_model->updateDataWhere($arrdata, 't_pinjam', array($kondisi => $id));
        }
    }

}
