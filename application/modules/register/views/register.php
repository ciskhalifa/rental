<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Register</b>Rental</a>
    </div>
    <!-- /.login-logo -->
    <div class="register-box-body">
        <p class="register-box-msg">Register Form</p>
        <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;"> </div> 
        <form role="form" id="xfrm" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="no_telp" class="form-control" placeholder="No Telepon">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <textarea name="alamat" class="form-control" placeholder="Alamat Lengkap"></textarea>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <select name="jk" class="form-control">
                    <option value="">- Pilihan -</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="Username">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <a href="<?= base_url('home'); ?>" class="btn btn-warning btn-block btn-flat">Back</a>
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>

                </div>
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->