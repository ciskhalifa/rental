<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Register extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'register';

        $this->load->view('layout_login_member', $data);
    }

    public function doRegister() {
        if (IS_AJAX) {
            $nama = $this->input->post('nama_lengkap');
            $no_telp = $this->input->post('no_telp');
            $alamat = $this->input->post('alamat');
            $jk = $this->input->post('jk');
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $idkonsumen = $this->Data_model->getLastIdDb('m_konsumen', 'kode');

            $konsumen = array(
                'nama_lengkap' => $nama,
                'no_telp' => $no_telp,
                'alamat' => $alamat,
                'jk' => $jk,
            );
            $users = array(
                'kode' => $idkonsumen['kode'],
                'nama_lengkap' => $nama,
                'username' => $username,
                'password' => $password,
                'role' => '4'
            );
            $res = $this->Data_model->simpanData($konsumen, 'm_konsumen');
            $res2 = $this->Data_model->simpanData($users, 'm_user');
            if ($res !== FALSE && $res2 !== FALSE) {
                echo base_url('home');
            } else {
                //echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button> Password / Username tidak ditemukan/salah.';
            }
        }
    }

}
