<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Laporan</h3>
                </div>
                <div class="box-body">
                    <table id="data-peminjaman" class="table table-bordered table-striped">
                        <thead>
                        <th>Laporan</th>
                        <th>Filter</th>
                        <th>Aksi</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="1">Laporan Semua Barang</td>
                                <td></td>
                                <td><a href="<?=base_url('laporan/cetakbarang');?>" target="_BLANK" class="btn btn-primary">Cetak</a></td>
                            </tr>
                            <tr>
                                <form method="POST" action="<?=base_url('laporan/cetakpeminjaman')?>">
                                    <td>Laporan Peminjaman</td>
                                    <td>Dari : <input type="text" class="tgldari" name="dari"> Sampai : <input type="text" class="tglsampai" name="sampai"></td>
                                    <td><button type="submit" class="btn btn-primary">Cetak</button></td>
                                </form>
                            </tr>
                            <tr>
                                <form method="POST" action="<?=base_url('laporan/cetakpengembalian')?>">
                                    <td>Laporan Pengembalian</td>
                                    <td>Dari : <input type="text" class="tgldari" name="dari"> Sampai : <input type="text" class="tglsampai" name="sampai"></td>
                                    <td><button type="submit" class="btn btn-primary">Cetak</button></td>
                                </form>
                            </tr>
                            <tr>
                                <form method="POST" action="<?=base_url('laporan/cetaktransaksitgl')?>">
                                    <td>Laporan Transaksi</td>
                                    <td>Dari : <input type="text" class="tgldari" name="dari"> Sampai : <input type="text" class="tglsampai" name="sampai"></td>
                                    <td><button type="submit" class="btn btn-primary">Cetak</button></td>
                                </form>
                            </tr>
                            <tr>
                                <td colspan="1">Laporan Pendapatan Denda</td>
                                <td></td>
                                <td><a href="<?=base_url('laporan/pendapatandenda');?>" target="_BLANK" class="btn btn-primary">Cetak</a></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="myConfirm" class="modal fade">
    <div class="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">

                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>