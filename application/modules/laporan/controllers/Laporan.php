<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        $this->load->library('pdf');
    }

    public function index()
    {
        $data['content'] = 'laporan';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("No", "Kode Pinjam", "Nama Lengkap", "Tanggal Sewa", "Tanggal Kembali", "Status", "Opsi");
        $this->load->view('default', $data);
    }

    public function cetakbarang()
    {
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $a = base_url('assets/admin/dist/img/avatar5.png');
        $pdf->Image($a, 90,0,90,0,'PNG');
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->Cell(190, 7, 'Laporan BARANG', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(190, 7, 'DAFTAR BARANG', 0, 1, 'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(30, 6, 'KATEGORI', 1, 0);
        $pdf->Cell(30, 6, 'MERK', 1, 0);
        $pdf->Cell(85, 6, 'NAMA BARANG', 1, 0);
        $pdf->Cell(35, 6, 'STOK', 1, 0);
        $pdf->Cell(45, 6, 'HARGA SEWA', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $q = $this->Data_model->jalankanQuery("SELECT
        m_barang.*,
        m_merk.`nama_merk`,
        m_kategori.`nama_kategori`
      FROM
        m_barang
        JOIN m_merk
          ON m_merk.`kode` = m_barang.`kode_merk`
          JOIN m_kategori ON m_kategori.`kode` = m_barang.`kode_kategori` ORDER BY m_barang.kode asc", 3);
        //print_r($q[0]); die;
        foreach ($q as $key => $row) {
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(30, 6, $row->nama_kategori, 1, 0);
            $pdf->Cell(30, 6, $row->nama_merk, 1, 0);
            $pdf->Cell(85, 6, $row->nama_barang, 1, 0);
            $pdf->Cell(35, 6, $row->stok, 1, 0);
            $pdf->Cell(45, 6, $row->harga_sewa, 1, 1);
        }
        $pdf->Output();
    }

    public function cetaktransaksitglx()
    {
        $dari = $this->input->post('dari');
        $sampai = $this->input->post('sampai');
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, "Laporan Transaksi Periode " . $dari . " - " . $sampai, 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 0);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 0);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vpinjam WHERE tgl_sewa BETWEEN '" . $dari . "' AND '" . $sampai . "'", 3);

        foreach ($q as $key => $row) {
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 1);
        }
        $pdf->Output();
    }
    public function cetaktransaksitgl()
    {
        $dari = $this->input->post('dari');
        $sampai = $this->input->post('sampai');
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        
        $pdf->Cell(190, 7, "Laporan Pendapatan Bersih", 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 0);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 0);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 0);
        $pdf->Cell(50, 6, 'TOTAL TRANSAKSI', 1, 0);
        $pdf->Cell(45, 6, 'DENDA', 1, 1);
        $pdf->SetFont('Arial', '', 10);

        $q = $this->Data_model->jalankanQuery("SELECT vdetail.*, SUM(jumlah * harga_sewa) AS total, SUM(bayar - kembalian) AS pembayaran  FROM vdetail WHERE tgl_sewa BETWEEN '" . $dari . "' AND '" . $sampai . "' AND status_pinjam='Dikembalikan' AND stat='Tidak Denda'", 3);

        foreach ($q as $key => $row) {
            $nilai[] = $row->total;
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 0);
            $pdf->Cell(50, 6, "Rp. " . number_format($row->total, 2, ',', '.'), 1, 1);
            $pdf->Cell(45, 6, "Rp. " . number_format($row->denda, 2, ',', '.'), 1, 1);
        }
        $pdf->ln();
        $pdf->ln();
        $pdf->Cell(60, 7, "TOTAL", 1, 0);
        $pdf->Cell(60, 7, "Rp. " . number_format(array_sum($nilai), 2, ',', '.'), 1, 1);
        $pdf->Output();
    }
    public function cetakpeminjaman()
    {
        $dari = $this->input->post('dari');
        $sampai = $this->input->post('sampai');
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, "Laporan Peminjaman", 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 0);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 0);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 0);
        $pdf->Cell(50, 6, 'STATUS', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vpinjam WHERE tgl_sewa BETWEEN '" . $dari . "' AND '" . $sampai . "' AND stat='Dipinjamkan'", 3);

        foreach ($q as $key => $row) {
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 0);
            $pdf->Cell(50, 6, $row->stat, 1, 1);
        }
        $pdf->Output();
    }

    public function cetakpengembalian()
    {
        $dari = $this->input->post('dari');
        $sampai = $this->input->post('sampai');
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, "Laporan Pengembalian", 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 0);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 0);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 0);
        $pdf->Cell(50, 6, 'STATUS', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $q = $this->Data_model->jalankanQuery("SELECT * FROM vpinjam WHERE tgl_sewa BETWEEN '" . $dari . "' AND '" . $sampai . "' AND stat='Dikembalikan'", 3);

        foreach ($q as $key => $row) {
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 0);
            $pdf->Cell(50, 6, $row->stat, 1, 1);
        }
        $pdf->Output();
    }

    public function pendapatanbersih()
    {
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, "Laporan Pendapatan Bersih", 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 0);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 0);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 0);
        $pdf->Cell(50, 6, 'TOTAL TRANSAKSI', 1, 1);
        $pdf->SetFont('Arial', '', 10);

        $q = $this->Data_model->jalankanQuery("SELECT vdetail.*, SUM(jumlah * harga_sewa) AS total, SUM(bayar - kembalian) AS pembayaran  FROM vdetail WHERE status_pinjam='Dikembalikan' AND stat='Tidak Denda'", 3);

        foreach ($q as $key => $row) {
            $nilai[] = $row->total;
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 0);
            $pdf->Cell(50, 6, "Rp. " . number_format($row->total, 2, ',', '.'), 1, 1);
        }
        $pdf->ln();
        $pdf->ln();
        $pdf->Cell(60, 7, "TOTAL", 1, 0);
        $pdf->Cell(60, 7, "Rp. " . number_format(array_sum($nilai), 2, ',', '.'), 1, 1);
        $pdf->Output();

    }

    public function pendapatandenda()
    {
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, "Laporan Pendapatan Denda", 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 0);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 0);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 0);
        $pdf->Cell(50, 6, 'TOTAL TRANSAKSI', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $q = $this->Data_model->jalankanQuery("SELECT vdetail.*, SUM(jumlah * harga_sewa) AS total, SUM(bayar - kembalian) AS pembayaran  FROM vdetail WHERE status_pinjam='Dikembalikan' AND stat='Denda'", 3);

        foreach ($q as $key => $row) {
            $nilai[] = $row->total;
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 0);
            $pdf->Cell(50, 6, "Rp. " . number_format($row->total, 2, ',', '.'), 1, 1);
        }
        $pdf->ln();
        $pdf->ln();
        $pdf->Cell(60, 7, "TOTAL", 1, 0);
        $pdf->Cell(60, 7, "Rp. " . number_format(array_sum($nilai), 2, ',', '.'), 1, 1);
        $pdf->Output();
    }
}
