<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Admin</b>Rental</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;"> </div> 
        <form role="form" id="xfrm" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="Username" maxlength="10">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password"  maxlength="10">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <select name="role" class="form-control" required>
                    <option value=""> Pilih Hak Akses </option>
                    <option value="1"> Admin </option>
                    <option value="2"> Pemilik </option>
                </select>
            </div>
            <div class="row">
                <div class="col-xs-8">
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>   
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->