<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = $arey['kode'];
}else {
    $cid = '';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form Peminjaman</h3>
                </div>
                <div class="box-body">
                    <input type="hidden" id="tabel" value="peminjaman">
                    <form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
                        <div class="form-body">
                            <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">                            
                            <input type="hidden" name="kode_konsumen" id="cid" value="<?php echo $_SESSION['kode']; ?>">                            
                            <div class="form-group">
                                <label class="col-md-2 label-control">Tanggal Sewa</label>
                                <div class="col-md-4">
                                    <input type="text" data-provide="datepicker" class="form-control input-sm date" name="tgl_sewa" id="tgl_sewa" value="<?= (isset($arey)) ? $arey['tgl_sewa'] : ''; ?>" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 label-control">Tanggal Kembali</label>
                                <div class="col-md-4">
                                    <input type="text" data-provide="datepicker" class="form-control input-sm date" name="tgl_kembali" id="tgl_kembali" value="<?= (isset($arey)) ? $arey['tgl_kembali'] : ''; ?>" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 label-control">No Telp</label>
                                <div class="col-md-2">
                                    <input type="text" disabled="" class="form-control input-sm date" name="no_telp" id="jaminan" value="<?= (isset($rowkonsumen)) ? $rowkonsumen->no_telp : ''; ?>" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 label-control">Jaminan</label>
                                <div class="col-md-2">
                                    <input type="text" disabled="" class="form-control input-sm date" name="no_identitas" id="jaminan" value="<?= (isset($rowkonsumen)) ? $rowkonsumen->no_identitas : ''; ?>" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <table class="table" id="barang" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:20px">No</th>
                                                <th>Kategori</th>
                                                <th>Merk</th>
                                                <th>Barang</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (empty($rowbarang)) { ?>
                                                <tr id="row1">
                                                    <td style="text-align:center">1</td>
                                                    <td>
                                                        <select class="pilihkategori form-control" id="selectkategori1" name="kode_kategori[]">
                                                            <option value="">- Pilihan -</option>
                                                            <?php
                                                            $n = (isset($arey)) ? $arey['kode_kategori'] : '';
                                                            $q = $this->Data_model->get_kategori();
                                                            foreach ($q as $row):
                                                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                                                ?>
                                                                <option value="<?= $row->kode; ?>" <?= $kapilih; ?>><?= $row->nama_kategori; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="pilihmerk form-control" id="selectmerk1" name="kode_merk[]"><option value="">- Pilihan -</option></select>
                                                    </td>
                                                    <td>
                                                        <select class="pilihbarang form-control" id="selectbarang1" name="kode_barang[]"><option value="">- Pilihan -</option></select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="tahun_lulus1">
                                                    </td>
                                                </tr>
                                            <?php } else if (isset($rowbarang)) { ?>

                                                <?php
                                                $no = 0;
                                                foreach ($rowbarang as $row):
                                                    ?>
                                                    <tr id="row<?php echo($no + 1); ?>" >
                                                        <td style="text-align:center"><?php echo($no + 1); ?></td>
                                                        <td>
                                                            <select class="pilihkategori form-control" id="selectkategori<?php echo($no + 1); ?>" data-default="<?php echo $row->kode_kategori; ?>" name="kode_kategori[]"></select>
                                                        </td>
                                                        <td>
                                                            <select class="pilihmerk form-control" id="selectmerk<?php echo($no + 1); ?>" data-default="<?php echo $row->kode_merk; ?>" name="kode_merk[]"></select>
                                                        </td>
                                                        <td>
                                                            <select name="kode_barang[]" id="selectbarang<?php echo($no + 1); ?>" data-default="<?php echo $row->kode_barang; ?>" class="select2 pilihbarang form-control" data-live-search="true">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="tahun_lulus<?php echo ($no + 1); ?>" value="<?php echo $row->jumlah; ?>">
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $no++;
                                                endforeach;
                                                ?>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3">
                                                    <button type="button" class="btn btn-danger removebarang">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-primary addbarang">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="javascript:" class="btn btn-primary" id="saveform"><i class="icon-check2"></i> Simpan</a>
                                <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


