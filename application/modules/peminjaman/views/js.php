<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>

<script src="<?=base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Select2 -->
<link rel="stylesheet" href="<?=base_url('assets/admin/bower_components/select2/dist/css/select2.min.css');?>">
<!-- Select2 -->
<script src="<?=base_url('assets/admin/bower_components/select2/dist/js/select2.full.min.js');?>"></script>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url('assets/admin/plugins/iCheck/all.css');?>">
<!-- iCheck 1.0.1 -->
<script src="<?=base_url('assets/admin/plugins/iCheck/icheck.min.js');?>"></script>
<script src="<?=base_url('assets/jquery.chained.min.js')?>"></script>
<script>
    var myApp = myApp || {};
    $(function () {
        var date = new Date();
        date.setDate(date.getDate());
        var enddate = new Date();
        enddate.setDate(date.getDate() + 7);
        $('#tgl_sewa').datepicker({
            format: 'yyyy-mm-dd',
            endDate: enddate,
            startDate: date
        });
        $('#tgl_kembali').datepicker({
            format: 'yyyy-mm-dd',
            startDate: date
        });
        $('input:radio[name="status_member"]').change(function () {
            if ($(this).is(':checked') && $(this).val() == 'member') {
                //document.getElementById('selectkonsumen').style.display = "";
                $.ajax({
                    type: 'POST',
                    url: 'getStatMember',
                    data: 'status=' + $(this).val(),
                    success: function (html) {
                        $("#kode_konsumen").html(html);
                    }
                });
            } else {
                //document.getElementById('selectkonsumen').style.display = "none";
                $.ajax({
                    type: 'POST',
                    url: 'getStatMember',
                    data: 'status=' + $(this).val(),
                    success: function (html) {
                        $("#kode_konsumen").html(html);
                    }
                });
            }
        });
        // $('.bayar').on('change', function () {
        //     var bayar = $(this).val();
        //     var totalbayar = $('.totalbayar').val();
        //     var kode_transaksi = $('.id_trans').val();
        //     var link = '../peminjaman/getBayar';
        //         var kembalian = bayar - totalbayar;
        //         $('.kembalian').val(kembalian);
        //         $.ajax({
        //             type: 'POST',
        //             url: link,
        //             data: {'id_trans': kode_transaksi, 'kembalian': kembalian, 'bayar': bayar},
        //             success: function (html) {
        //                 location.reload(true);
        //                 document.getElementById('bayar').disabled = true;
        //             }
        //         });

        // });
        $('#kode_konsumen').on('change', function () {
            var id_konsumen = $(this).val();
            $.ajax({
                type: 'POST',
                url: 'getJaminan',
                data: 'id_konsumen=' + id_konsumen,
                success: function (html) {
                    $("#jaminan").val(html);
                }
            });
        });
        $('.select').select2({width: '100%'});
        $('#data-peminjaman').DataTable();
        $('#data-peminjaman').on('click', '.btnDel', function () {
            $('#getto').val($(this).attr("data-get"));
            $('#aepYes').text('Hapus');
            $("#cid").val($(this).attr("data-id"));
            $("#cod").val($(this).attr("data-mtabel"));
            $('.lblModal').text('hapus ' + $(this).attr("data-default"));
            $('#myConfirm').modal();
        });
        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=pinjam", dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    notify("Delete berhasil", "danger")
                    $("#myConfirm").modal("hide")
                    location.reload(true);
                }})
        });
        $("#file").on("change", function () {
            var file = this.files[0];
            var fileName = file.name;
            var fileType = file.type;
            console.log(fileType);
            if (fileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || fileType == "application/vnd.ms-excel") {
                $('#label').text(fileName);
                $('#reset').removeClass('hidden');
            } else {
                alert("Maaf format dokumen tidak sesuai");
                $(this).val('');
                $('#label').text('Pilih File');
            }
        });
        $("#tmblBatal").on("click", function () {
            window.location.replace("<?=base_url('peminjaman');?>");
        });
        $("#saveform").on("click", function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            }
            console.log($('#xfrm')[0]);
            var link = 'simpanData'
            var sData = new FormData($('#xfrm')[0]);
            $.ajax({
                url: link,
                type: "POST",
                data: sData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "html",

                beforeSend: function () {

                },
                success: function (html) {
                    window.location.replace('peminjaman');
                }
            });
            return false;
        });

        $('#tgl_kembali').on('change', function(){
            var tgl_sewa = new Date($('#tgl_sewa').val());
            var tgl_kembali = new Date($(this).val());
            var timeDiff = Math.abs(tgl_kembali.getTime() - tgl_sewa.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            $('#hari1').val(diffDays);
        });
        var a = $("#barang tbody tr").length;
        var b = $("#detailbarang tbody tr").length;
        
        $(".addbarang").on("click", function(){
            b++;
            var selected = $('.pilihbarang').find('option:selected');
            var subtotal = selected.data('harga') * $('#hari1').val() * $('#jumlah1').val(); 
            var z = '<td style=""width: 10px;><input name="kode_barang[]" readonly type="text" value="' + selected.val() + '"></td>';
            z += '<td>' + selected.data('barang') + '</td>';
            z += '<td><input name="jumlah[]" readonly type="text" value="' + $('#jumlah1').val() + '" id="jumlah' + (b) + '"></td>'; 
            z += '<td><input name="hari[]" readonly type="text" value="' + $('#hari1').val() + '" id="hari' + (b) + '"></td>'; 
            z += '<td>'+ selected.data('harga') +'</td>'; 
            z += '<td>'+ subtotal +'</td>';
            $("#detailbarang tbody").append('<tr id="row' + (b) + '">' + z + "</tr>");
            $('.total').val(sumColumn(5));
           
        });
        
        $('.bayar').on('change', function(){
            var itung = $(this).val() - $('.total').val();
            if ($(this).val() < 0 || itung < 0){
                alert('Error');
            }else{
                $('.kembalian').val(itung);    
            }
        });
       
        function sumColumn(index) {
          var total = 0;
          $("td:nth-child(" + index + ")").each(function() {
            total += parseInt($(this).text(), 10) || 0;
          });  
          return total;
        }
        // $(".addbarang").on("click", function () {
        //     a++;
        //     var x = '<td style="text-align: center;">' + (a) + "</td>";
        //     x += '<td><select class="select2 pilihkategori form-control" id="selectkategori' + (a) + '" name="kode_kategori[]"></select></td>';
        //     x += '<td><select class="select2 pilihmerk form-control" id="selectmerk' + (a) + '" name="kode_merk[]"></select></select></td>';
        //     x += '<td><select class="select2 pilihbarang form-control" id="selectbarang' + (a) + '" name="kode_barang[]"></select></td>';
        //     x += '<td><input type="text" class="form-control" placeholder="Jumlah Barang" name="jumlah[]" id="jumlah' + (a) + '"></td>';
        //     $("#barang tbody").append('<tr id="row' + (a) + '">' + x + "</tr>");
        //     //$("#selectmerk" + a).chained("#selectkategori" + a);
        //     selectKategori(a);
        //     kategori(a);
        //     merk(a);
        //     var u = a-1;
        //     var namabarang = $('#selectbarang' + u).data('namabarang');
        //     var z = '<td>' + namabarang+ '</td>';
        //     z += '<td>'+ $('#jumlah' + (a)).val()+'</td>';
            
        //     $("#detailbarang tbody").append('<tr id="row' + (b) + '">' + z + "</tr>");
        // });
        $(".removebarang").on("click", function () {
            if ($("#detailbarang tbody tr").length > 0) {
                $("#detailbarang tbody tr:last-child").remove();
                a--;
                $('.total').val(sumColumn(5));
            } else {
                alert("Baris Habis");
            }
        });
        kategori(1);
        merk(1);
        function kategori(urutan) {
            $('#selectkategori' + urutan).on('change', function () {
                var kategori_id = $(this).val();
                if (kategori_id) {
                    $.ajax({
                        type: 'POST',
                        url: 'getMerk',
                        data: 'kategori_id=' + kategori_id,
                        success: function (html) {
                            $("#selectmerk" + urutan).html(html);
                            $("#selectbarang" + urutan).html('<option value="">- Pilihan -</option>');
                        }
                    });
                } else {
                    $("#selectmerk" + urutan).html('<option value="">- Pilihan -</option>');
                    $("#selectbarang" + urutan).html('<option value="">- Pilihan -</option>');
                }
            });
        }
        function merk(urutan) {
            $('#selectmerk' + urutan).on('change', function () {
                var merk_id = $(this).val();
                if (merk_id) {
                    $.ajax({
                        type: 'POST',
                        url: 'getBarang',
                        data: 'merk_id=' + merk_id,
                        success: function (html) {
                            $("#selectbarang" + urutan).html(html);
                        }
                    });
                } else {
                    $("#selectbarang" + urutan).html('<option value="">- Pilihan -</option>');
                }
            });
        }
        function selectKategori(urutan) {
            $.ajax({
                url: "peminjaman/select_kategori",
                type: "POST",
                success: function (html) {
                    json = eval(html);
                    $("#selectkategori" + urutan + " ").append('<option value="">Pilihan</option>');
                    $(json).each(function () {
                        $("#selectkategori" + urutan + " ").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                    });
                    $(".select").select2();
                }
            })
        }

    });
    function myFunction() {
        window.print();
    }
</script>