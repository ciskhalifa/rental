<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Peminjaman extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("No", "Kode Pinjam", "Nama Lengkap", "Tanggal Sewa", "Tanggal Kembali", "Status", "Opsi");
        if ($_SESSION['role'] == '4') {
            $data['content'] = 'peminjaman_member';
            $sTable = 'vpinjam';
            $sWhere = " kode_konsumen = " . $_SESSION['kode'];
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE $sWhere ";
        } else {
            $data['content'] = 'peminjaman';
            $sTable = 'vpinjam';
            $sWhere = "";
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere ";
        }
        $data['rowdata'] = $this->Data_model->jalankanQuery($tQuery, 3);
        $this->load->view('default', $data);
    }

    public function form()
    {
        $nilai = $this->uri->segment(3);
        $data['content'] = 'peminjaman/form_barang';
        $data['css'] = 'css';
        $data['js'] = 'js';
        if ($nilai != "") {
            $data['rowbarang'] = $this->Data_model->selectData('detail_pinjam', 'kode_pinjam', array('kode_pinjam' => $this->uri->segment(3)));
            $data['rowdata'] = $this->Data_model->satuData('t_pinjam', array('kode' => $nilai));
            $data['rowkonsumen'] = $this->Data_model->satuData('m_konsumen', array('kode' => $data['rowdata']->kode_konsumen));
        }
        $this->load->view('default', $data);
    }

    public function formmember()
    {
        $nilai = $this->uri->segment(3);
        $data['content'] = 'peminjaman/form_barang_member';
        $data['css'] = 'css';
        $data['js'] = 'js';
        if ($nilai != "") {
            $data['rowbarang'] = $this->Data_model->selectData('detail_pinjam', 'kode_pinjam', array('kode_pinjam' => $this->uri->segment(3)));
            $data['rowdata'] = $this->Data_model->satuData('t_pinjam', array('kode' => $nilai));
        }
        $data['rowkonsumen'] = $this->Data_model->satuData('m_konsumen', array('kode' => $_SESSION['kode']));
        $this->load->view('default', $data);
    }

    public function getBayar()
    {
        $id = $_POST['id_trans'];
        $kondisi = 'kode';
        $arrdata = array('bayar' => $_POST['bayar'], 'kembalian' => $_POST['kembalian']);
        $this->Data_model->updateDataWhere($arrdata, 't_pinjam', array($kondisi => $id));
    }
    
    public function simpanData()
    {
        // print_r($this->input->post());
        // die;
        $arrdata = array();
        $arrbarang = array();
        $cid = '';
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {

            } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                switch (trim($key)):
            case 'cid':
                $cid = $value;
                break;
            case 'kode_barang':
            case 'jumlah':
            case 'jaminan':
            case 'status_member':
            case 'total':
                break;
            case 'status':
                $arrdata['status'] = "Dipinjamkan";
                break;
            default:
                if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("-", $value);
                            $newtgl = $tgl[1] . "-" . $tgl[0] . "-" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = $value; // date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                } else {
                    $arrdata[$key] = $value;
                }
                break;
                endswitch;
            }
        }

        if ($cid == "") {
            $arrdata['status'] = "Dipinjamkan";
            $this->Data_model->simpanData($arrdata, 't_pinjam');
            $id_pinjam = $this->Data_model->getLastIdDb('t_pinjam', 'kode');
            if (is_array($this->input->post('kode_barang'))) {
                for ($i = 0; $i < count($this->input->post('kode_barang')); $i++) {
                    $arrbarang['kode_pinjam'] = $id_pinjam['kode'];
                    $arrbarang['kode_barang'] = $this->input->post('kode_barang')[$i];
                    $arrbarang['jumlah'] = $this->input->post('jumlah')[$i];
                    $this->Data_model->simpanData($arrbarang, 'detail_pinjam');
                }
            } else {
                $arrbarang['kode_pinjam'] = $this->input->post('kode_pinjam')[0];
                $arrbarang['kode_barang'] = $this->input->post('kode_barang')[0];
                $arrbarang['jumlah'] = $this->input->post('jumlah')[0];
                $this->Data_model->simpanData($arrbarang, 'detail_pinjam');
            }
        } else {
            $kondisi = 'kode';
            if (is_array($this->input->post('kode_pinjam'))) {
                $q = "DELETE FROM m_barang WHERE kode IN (" . $this->input->post('kode') . ")";
                $this->Data_model->jalankanQuery($q);
                for ($i = 0; $i < count($this->input->post('kode_barang')); $i++) {
                    $arrbarang['kode_pinjam'] = $this->input->post('kode_pinjam');
                    $arrbarang['kode_barang'] = $this->input->post('kode_barang')[$i];
                    $arrbarang['jumlah'] = $this->input->post('jumlah')[$i];
                    $this->Data_model->simpanData($arrbarang, 'detail_pinjam');
                }
            }
            $this->Data_model->updateDataWhere($arrdata, 't_pinjam', array($kondisi => $cid));
        }
    }

    public function detail($id)
    {

        $data['css'] = 'css';
        $data['js'] = 'js';
        if ($_SESSION['role'] == '4') {
            $data['content'] = 'peminjaman/detail_member';
        } else {
            $data['content'] = 'peminjaman/detail';
        }
        $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vdetail WHERE kode=" . $id, 3);
        $data['total'] = $this->Data_model->jalankanQuery("SELECT SUM(jumlah * harga_sewa) as tot FROM vdetail WHERE kode= " . $id, 3);
        $this->load->view('default', $data);
    }

    public function getJaminan()
    {
        $id = $_POST['id_konsumen'];
        $q = $this->Data_model->jalankanQuery("SELECT no_identitas FROM m_konsumen WHERE kode=" . $id, 3);
        echo $q[0]->no_identitas;
    }

    public function getStatMember()
    {
        $id = $_POST['status'];
        $q = $this->Data_model->jalankanQuery('SELECT * FROM m_konsumen WHERE status="' . $id . '"', 3);
        if (count($q) > 0) {
            echo '<option value="">- Pilihan -</option>';
            foreach ($q as $row) {
                echo '<option value="' . $row->kode . '">' . $row->nama_lengkap . '</option>';
            }
        } else {
            echo '<option value="">- Data Tidak Ditemukan -</option>';
        }
    }

    public function getMerk()
    {
        $id = $_POST['kategori_id'];
        $q = $this->Data_model->jalankanQuery("SELECT * FROM m_merk WHERE kode_kategori =" . $id, 3);
        if (count($q) > 0) {
            echo '<option value="">- Pilihan -</option>';
            foreach ($q as $row) {
                echo '<option value="' . $row->kode . '">' . $row->nama_merk . '</option>';
            }
        } else {
            echo '<option value="">- Data Tidak Ditemukan -</option>';
        }
    }

    public function getBarang()
    {
        $id = $_POST['merk_id'];
        $q = $this->Data_model->jalankanQuery("SELECT * FROM m_barang WHERE kode_merk =" . $id, 3);
        if (count($q) > 0) {
            echo '<option value="">- Pilihan -</option>';
            foreach ($q as $row) {
                echo '<option value="' . $row->kode . '" data-barang="'.$row->nama_barang.'" data-harga="'.$row->harga_sewa.'">' . $row->nama_barang . '</option>';
            }
        } else {
            echo '<option value="">- Data Tidak Ditemukan -</option>';
        }
    }

    public function select_barang()
    {
        $query = "SELECT kode, nama_barang AS disp, stok FROM m_barang";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }

    public function select_kategori()
    {
        $query = "SELECT kode, nama_kategori AS disp FROM m_kategori";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }

    public function select_merk()
    {
        $query = "SELECT kode, nama_merk AS disp FROM m_merk";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }

    public function getListBarang()
    {
        $tabel = $this->input->post('tabel');
        echo $tabel;
        die;
        $param = $this->input->post('param');
        $field = $this->input->post('fld');
        if (strpos($this->input->post('kolom'), ',')) {
            $arrnmfld = explode(',', $this->input->post('kolom'));
            $nmfld1 = $arrnmfld[0];
            $nmfld2 = $arrnmfld[1];
            $grpby = array($nmfld1, $nmfld2);
        } else {
            $nmfld1 = $this->input->post('kolom');
            $nmfld2 = $this->input->post('kolom');
            $grpby = array($nmfld1);
        }
        if (strpos($this->input->post('fld'), ',')) {
            $f = explode(",", $field);
            $p = explode("-", $param);
            $kondisi = array($f[0] => $p[0], $f[1] => $p[1]);
        } else {
            $kondisi = array($field => $param);
        }
        $order = "1";
        $query = $this->db->select("$nmfld1,$nmfld2")->order_by($order, 'ASC')->group_by($grpby)->get_where($tabel, $kondisi);

        $result = $query->result();
        $data = array();
        if (!empty($result)) {
            foreach ($result as $row) {
                $data[] = '{"id" : "' . $row->$nmfld1 . '", "value" : "' . trim($row->$nmfld2) . '", "nomor": "' . $row->$nmfld1 . '", "sub":0, "isigrup":0}';
            }
        }
        if (IS_AJAX) {
            echo '[' . implode(',', $data) . ']';
            // die;
        }
    }

    public function updateData()
    {
        $kode = $this->uri->segment(3);
        $tabel = 't_datin';
        $kondisi = array('kode' => $kode);
        if (IS_AJAX) {
            $arrdata = array();
            foreach ($this->input->post() as $key => $value) {
                $arrdata[$key] = $value;
            }
            $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
        }
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('t_pinjam', $kondisi);
                $this->Data_model->hapusDataWhere('detail_pinjam', array('kode_pinjam' => $param));
                echo json_encode("ok");
            }
        }
    }

}
