<?php $this->load->view('home/' . $head);?>
    <!-- Team Section Start -->
    <section id="team" class="section-padding bg-gray " data-wow-delay="1.2s">
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.1s">All Products</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.1s"></div>
        </div>
        <!-- Control buttons -->
        <div id="myBtnContainer">
          <button class="btn btn-common" onclick="filterSelection('Eiger')"> Eiger</button>
          <button class="btn btn-common" onclick="filterSelection('Consina')"> Consina</button>
          <button class="btn btn-common" onclick="filterSelection('Deuter')"> Deuter</button>
          <button class="btn btn-common" onclick="filterSelection('Rei')"> rei</button>
        </div>
        <div class="row">
          <?php
          $q = $this->Data_model->jalankanQuery('SELECT m_barang.*, m_kategori.nama_kategori, m_merk.nama_merk FROM m_barang JOIN m_kategori ON m_kategori.kode = m_barang.kode_kategori JOIN m_merk ON m_merk.kode = m_barang.kode_merk', 3);
          foreach ($q as $row):
            $namakat = $row->nama_merk;
          ?>
          <div class="col-lg-6 col-md-12 col-xs-12 filterDiv <?= $namakat ?>">
            <!-- Team Item Starts -->
            <div class="team-item">
              <div class="team-img">
                <img class="img-fluid" src="<?=base_url('publik/barang/') . $row->gambar?>" alt="" style="height:200px; width:200px;">
              </div>
              <div class="contetn">
                <div class="info-text">
                  <h3><a href="#"><?=$row->nama_kategori?></a></h3>
                  <p><?=$row->nama_barang?></p>
                  <p><?="Rp. " . number_format($row->harga_sewa, 2, ',', '.') . "/ Hari";?></p>
                </div>
                <p><?=$row->desc?></p>
              </div>
            </div>
            
            <!-- Team Item Ends -->
          </div>
          <?php endforeach;?>
        </div>
      </div>
    </section>
    <!-- Team Section End -->
    <!-- Contact Section Start -->
    <section id="contact" class="section-padding bg-gray">
      <div class="container">
        <div class="section-header text-center">
          <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Countact Us</h2>
          <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
        </div>
        <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.3s">
          <div class="col-lg-7 col-md-12 col-sm-12">
            <div class="contact-block">
              <form id="contactForm">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" required data-error="Please enter your name">
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" placeholder="Email" id="email" class="form-control" name="email" required data-error="Please enter your email">
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                   <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" placeholder="Subject" id="msg_subject" class="form-control" required data-error="Please enter your subject">
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea class="form-control" id="message" placeholder="Your Message" rows="7" data-error="Write your message" required></textarea>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="submit-button text-left">
                      <button class="btn btn-common" id="form-submit" type="submit">Send Message</button>
                      <div id="msgSubmit" class="h3 text-center hidden"></div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-5 col-md-12 col-xs-12">
            <div class="map">
              <object style="border:0; height: 280px; width: 100%;" data="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d34015.943594576835!2d-106.43242624069771!3d31.677719472407432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86e75d90e99d597b%3A0x6cd3eb9a9fcd23f1!2sCourtyard+by+Marriott+Ciudad+Juarez!5e0!3m2!1sen!2sbd!4v1533791187584"data="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d34015.943594576835!2d-106.43242624069771!3d31.677719472407432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86e75d90e99d597b%3A0x6cd3eb9a9fcd23f1!2sCourtyard+by+Marriott+Ciudad+Juarez!5e0!3m2!1sen!2sbd!4v1533791187584"></object>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Contact Section End -->
    <?php $this->load->view('home/' . $footer);?>