<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Home extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['head'] = 'header';
        $data['footer'] = 'footer';

        $this->load->view('index', $data);
    }
}
