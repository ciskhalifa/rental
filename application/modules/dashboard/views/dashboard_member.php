<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Barang</h3>
                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url('cart'); ?>" class="cart-link" title="View Cart">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <span>(<?php echo $this->cart->total_items(); ?>)</span>
                        </a>
                    </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                    <?php if (!empty($products)) {foreach ($products as $row) {?>
                        <div class="col-sm-4 col-lg-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<?php echo base_url('publik/barang/' . $row->gambar); ?>" style="height:320px;"/>
                                <div class="caption">
                                    <h4 class="pull-right">Rp. <?php echo $row->harga_sewa; ?></h4>
                                    <h4><?php echo $row->nama_barang; ?></h4>
                                    <p>Stok Tersedia : <?php echo $row->stok; ?></p>
                                </div>
                                <div class="atc">
                                    <a href="<?php echo base_url('cart/addToCart/' . $row->kode); ?>" class="btn btn-success">
                                        Add to Cart
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php }} else {?>
                        <p>Product(s) not found...</p>
                    <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
