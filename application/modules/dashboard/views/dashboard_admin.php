<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $countkonsumen[0]->countdata; ?></h3>
                    <p>Total Konsumen</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $countpinjam[0]->countdata; ?></h3>

                    <p>Total Order</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $countselesai[0]->countdata; ?></h3>

                    <p>Transaksi Selesai</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countdipinjam[0]->countdata; ?></h3>

                    <p>Transaksi Belum Selesai</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Barang</h3>
                </div>
                <div class="box-body">
                    <table id="data-barang" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </thead>    
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($rowdata as $row):
                                ?>
                                <tr>
                                    <td><?= $row->kode; ?></td>
                                    <td><?= $row->nama_barang; ?></td>
                                    <td><?= $row->stok; ?></td>
                                    <td><?= $row->harga_sewa; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
    </div>
</section>


