<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        if ($_SESSION['role'] == 1) {
            $data['content'] = 'dashboard_admin';
            $data['css'] = 'css_admin';
            $data['js'] = 'js_admin';
            $data['countkonsumen'] = $this->Data_model->jalankanQuery('SELECT count(*) as countdata FROM m_konsumen', 3);
            $data['countpinjam'] = $this->Data_model->jalankanQuery('SELECT count(*) as countdata FROM t_pinjam', 3);
            $data['countselesai'] = $this->Data_model->jalankanQuery("SELECT count(*) as countdata FROM t_pinjam WHERE status= 'Dikembalikan'", 3);
            $data['countdipinjam'] = $this->Data_model->jalankanQuery("SELECT count(*) as countdata FROM t_pinjam WHERE status= 'Dipinjamkan'", 3);
            $data['kolom'] = array("Kode", "Nama Barang", "Stok", "Harga Sewa");
            $data['rowdata'] = $this->Data_model->jalankanQuery('SELECT * FROM m_barang', 3);
        } else if ($_SESSION['role'] == 2) {
            $data['content'] = 'dashboard_operasional';
            $data['css'] = 'css_operasional';
            $data['js'] = 'js_operasional';
        } else if ($_SESSION['role'] == 3) {
            $data['content'] = 'dashboard_pemilik';
            $data['css'] = 'css_pemilik';
            $data['js'] = 'js_pemilik';
        } else if ($_SESSION['role'] == 4) {
            $data['products'] = $this->Data_model->jalankanQuery("SELECT * FROM m_barang", 3);
            $data['content'] = 'dashboard_member';
            $data['css'] = 'css_member';
            $data['js'] = 'js_member';
        } else {
            $data['products'] = $this->Data_model->jalankanQuery("SELECT * FROM m_barang", 3);
            $data['content'] = 'dashboard_member';
            $data['css'] = 'css_member';
            $data['js'] = 'js_member';
        }
        $this->load->view('default', $data);
    }

    public function listData()
    {
        /*
         * list data
         */
        if (IS_AJAX) {
            $aColumns = array("no", "ip", "nama_client", "alamat", "status", "tgl_data", "tgl_modify", "kode");
            $sIndexColumn = "kode";
            $sTable = 'vclient';
            $sTablex = '';
            $sWhere = " AND kode_perusahaan = '" . $this->uri->segment(4) . '"';
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere ";
            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
        }
    }

}
