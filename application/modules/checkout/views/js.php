<script>
 $(function () {
        var date = new Date();
        date.setDate(date.getDate());
        var enddate = new Date();
        enddate.setDate(date.getDate() + 7);
        $('#tgl_sewa').datepicker({
            format: 'yyyy-mm-dd',
            startDate: date,
            endDate: enddate
        }
        );
        $('#tgl_kembali').datepicker({
            format: 'yyyy-mm-dd',
            startDate: date
        });
 });
</script>
