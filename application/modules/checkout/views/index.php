<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">

                    <!-- Shipping address -->
                    <form class="form-horizontal" method="post" action="<?=base_url('checkout/simpan')?>">
                    <div class="ship-info">
                        <h4>Customer Info</h4>
                        <div class="form-group">
                            <label class="control-label col-sm-2">No. Identitas:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="name" value="<?php echo !empty($custData->no_identitas) ? $custData->no_identitas : ''; ?>" placeholder="Enter name" disabled>
                                <?php echo form_error('name', '<p class="help-block error">', '</p>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Nama Lengkap:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="name" value="<?php echo !empty($custData->nama_lengkap) ? $custData->nama_lengkap : ''; ?>" placeholder="Enter name" disabled>
                                <?php echo form_error('name', '<p class="help-block error">', '</p>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Alamat:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="alamat" value="<?php echo !empty($custData->alamat) ? $custData->alamat : ''; ?>" placeholder="Enter name" disabled>
                                <?php echo form_error('name', '<p class="help-block error">', '</p>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">No Telepon:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="alamat" value="<?php echo !empty($custData->no_telp) ? $custData->no_telp : ''; ?>" placeholder="Enter name" disabled>
                                <?php echo form_error('name', '<p class="help-block error">', '</p>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="control-label col-sm-2">Tanggal Sewa:</label>
                                <div class="col-md-2">
                                    <input type="text" data-provide="datepicker" class="form-control input-sm date" name="tgl_sewa" id="tgl_sewa" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Tanggal Kembali:</label>
                                <div class="col-md-2">
                                    <input type="text" data-provide="datepicker" class="form-control input-sm date" name="tgl_kembali" id="tgl_kembali" value="" data-error="wajib diisi" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                    </div>
                    <hr>
                    <br>
                    <h3 class="box-title">Order Preview</h3>
                    <table class="table">
                    <thead>
                        <tr>
                            <th width="13%"></th>
                            <th width="34%">Nama Barang</th>
                            <th width="18%">Harga Sewa</th>
                            <th width="13%">Jumlah</th>
                            <th width="22%">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($this->cart->total_items() > 0) {foreach ($cartItems as $item) {?>
                        <tr>
                            <td>
                                <?php $imageURL = !empty($item["gambar"]) ? base_url('publik/barang/' . $item["gambar"]) : base_url('assets/images/pro-demo-img.jpeg');?>
                                <img src="<?php echo $imageURL; ?>" width="75"/>
                            </td>
                            <td><?php echo $item["name"]; ?></td>
                            <td><?php echo 'Rp. ' . $item["price"]; ?></td>
                            <td><?php echo $item["qty"]; ?></td>
                            <td><?php echo 'Rp. ' . $item["subtotal"]; ?></td>
                        </tr>
                        <?php }} else {?>
                        <tr>
                            <td colspan="5"><p>No items in your cart...</p></td>
                        </tr>
                        <?php }?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4"></td>
                            <?php if ($this->cart->total_items() > 0) {?>
                            <td class="text-center">
                                <strong>Total <?php echo 'Rp. ' . $this->cart->total(); ?></strong>
                            </td>
                            <?php }?>
                        </tr>
                    </tfoot>
                    </table>
                    <div class="footBtn">
                        <a href="<?php echo base_url('cart/'); ?>" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Back to Cart</a>
                        <button type="submit" name="placeOrder" class="btn btn-success orderBtn">Place Order <i class="glyphicon glyphicon-menu-right"></i></button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>