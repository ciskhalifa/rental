<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkout extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load form library & helper
        $this->load->library('form_validation');
        $this->load->helper('form');
        // Load cart library
        $this->load->library('cart');
        $this->load->library('pdf');

    }

    public function index()
    {
        // Redirect if the cart is empty
        if ($this->cart->total_items() <= 0) {
            redirect('dashboard');
        }
        $data = array();
        $data['content'] = 'index';
        $data['css'] = 'css';
        $data['js'] = 'js';
        // Customer data
        $data['custData'] = $this->Data_model->satuData('m_konsumen', array('kode' => $_SESSION['kode']));
        // Retrieve cart data from the session
        $data['cartItems'] = $this->cart->contents();
        // Pass products data to the view
        $this->load->view('default', $data);
    }

    public function simpan()
    {
        $arrdata = array();
        $arrbarang = array();
        $arrdata['kode_konsumen'] = $_SESSION['kode'];
        $arrdata['status'] = "Dipinjamkan";
        $arrdata['tgl_sewa'] = $this->input->post('tgl_sewa');
        $arrdata['tgl_kembali'] = $this->input->post('tgl_kembali');
        $insertOrder = $this->Data_model->simpanData($arrdata, 't_pinjam');
        $id_pinjam = $this->Data_model->getLastIdDb('t_pinjam', 'kode');
        // Retrieve cart data from the session
        $cartItems = $this->cart->contents();
        // Cart items
        $i = 0;
        foreach ($cartItems as $rowid => $item) {
            $arrbarang['kode_pinjam'][$i] = $id_pinjam['kode'];
            $arrbarang['kode_barang'][$i] = $item['id'];
            $arrbarang['jumlah'][$i] = $item['qty'];
            $i++;
        }
        if (!empty($arrbarang)) {
            // Insert order items
            if (is_array($arrbarang)) {
                for ($x = 0; $x < count($arrbarang['kode_pinjam']); $x++) {
                    $data = array(
                        'kode_pinjam' => $arrbarang['kode_pinjam'][$x],
                        'kode_barang' => $arrbarang['kode_barang'][$x],
                        'jumlah' => $arrbarang['jumlah'][$x],
                    );
                    $this->Data_model->simpanData($data, 'detail_pinjam');
                    $this->cart->destroy();
                }
            } else {
                $data = array(
                    'kode_pinjam' => $arrbarang['kode_pinjam'][0],
                    'kode_barang' => $arrbarang['kode_barang'][0],
                    'jumlah' => $arrbarang['jumlah'][0],
                );
                $this->Data_model->simpanData($data, 'detail_pinjam');
                $this->cart->destroy();
            }
        }
        redirect('peminjaman');
    }

    public function print_struk($kode)
    {
        $pdf = new FPDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, "NOTA", 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'KODE', 1, 0);
        $pdf->Cell(85, 6, 'NAMA LENGKAP', 1, 1);
        $pdf->Cell(35, 6, 'TANGGAL SEWA', 1, 1);
        $pdf->Cell(45, 6, 'TANGGAL KEMBALI', 1, 1);
        $pdf->Cell(50, 6, 'TOTAL TRANSAKSI', 1, 1);
        $pdf->SetFont('Arial', '', 10);

        $q = $this->Data_model->jalankanQuery("SELECT vdetail.*, SUM(jumlah * harga_sewa) AS total, SUM(bayar - kembalian) AS pembayaran  FROM vdetail WHERE kode=$kode", 3);

        foreach ($q as $key => $row) {
            $nilai[] = $row->total;
            $pdf->Cell(20, 6, $row->kode, 1, 0);
            $pdf->Cell(85, 6, $row->nama_lengkap, 1, 0);
            $pdf->Cell(35, 6, $row->tgl_sewa, 1, 0);
            $pdf->Cell(45, 6, $row->tgl_kembali, 1, 0);
            $pdf->Cell(50, 6, "Rp. " . number_format($row->total, 2, ',', '.'), 1, 1);
        }
        $pdf->ln();
        $pdf->ln();
        $pdf->Cell(60, 7, "TOTAL", 1, 0);
        $pdf->Cell(60, 7, "Rp. " . number_format(array_sum($nilai), 2, ',', '.'), 1, 1);
        $pdf->Output();
    }
}
